#include "Canoa.hpp"
#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "Objeto.hpp"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "ResultadoAcao.hpp"
#include "Orientacao.hpp"
#include "ponto.h"

using namespace std;

//INICIALIZACAO DAS VARIAVEIS ESTATICAS

short Canoa::canoasJogador1 = 0;
short Canoa::canoasJogador2 = 0;

//CONSTRUTOR

Canoa::Canoa( int8_t jogador , ponto posicao0 , Orientacao direcao )
:Embarcacao( jogador , posicao0 , direcao , _Flags_Objeto::CASAS_CANOA , _Flags_Objeto::VIDAS_CANOA , _Flags_Objeto::CANOA ){
    switch( jogador ){
        case JOGADOR_1:
            canoasJogador1++;
            break;
        case JOGADOR_2:
            canoasJogador2++;
            break;
    }
}

Canoa::~Canoa(){
    switch( getJogador() ){
        case JOGADOR_1:
            canoasJogador1--;
            break;
        case JOGADOR_2:
            canoasJogador2--;
            break;
    }
}

//METODOS ACESSORES

short Canoa::getCanoasJogador1(){
    return canoasJogador1;
}

short Canoa::getCanoasJogador2(){
    return canoasJogador2;
}
