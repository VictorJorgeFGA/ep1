#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "Objeto.hpp"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "Orientacao.hpp"
#include "ponto.h"
#include "ResultadoAcao.hpp"

#include "_EXCECAO_STD_ForaDeIntervalo.hpp"

#include <vector>
#include <cstdio>
#include <cstdlib>

using namespace std;

//Inicializacao de variaveis estaticas

short Embarcacao::embarcacoesJogador1 = 0;
short Embarcacao::embarcacoesJogador2 = 0;

short Embarcacao::embarcacoesVivasJogador1 = 0;
short Embarcacao::embarcacoesVivasJogador2 = 0;

void Embarcacao::setEstadoVida( bool estadoVida ){
    switch( getJogador() )
    {
        case JOGADOR_1:
            if( estadoVida == false )
                embarcacoesVivasJogador1--;

            else if( estadoVida == true )
                embarcacoesVivasJogador1++;
            break;

        case JOGADOR_2:
            if( estadoVida == false )
                embarcacoesVivasJogador2--;

            else if( estadoVida == true )
                embarcacoesVivasJogador2++;

            break;
    }
    this->estadoVida = estadoVida;
}

//METODOS UTILITARIOS
short Embarcacao::procurarCasaCorrespondente( ponto posicaoMatriz )const{
    for( int indiceCasa = 0 ; indiceCasa < getQuantidadeCasasOnline() ; indiceCasa++ ){
        if( casa[ indiceCasa ]->getPosicaoMatriz() == posicaoMatriz  ){
            return indiceCasa;
        }
    }
    //Se chegou ate aqui, erro.
    return -1;
}

bool Embarcacao::verificarIndiceCasa( short indiceCasa )const{
    if( indiceCasa < 0 || indiceCasa >= getQuantidadeCasasOnline() )
        return false;
    else
        return true;
}

void Embarcacao::construirCasas( short quantidadeCasas , short vidasCasa ){
    printf("Criando CasasEmbarcacao:\n");

    for( int i = 0 ; i < quantidadeCasas ; i++ ){

        short linha = posicao0.first;
        short coluna = posicao0.second;   

        switch( direcao ){
            case DIREITA:
                coluna += i;
                break;
            case BAIXO:
                linha += i;
                break;
            case ESQUERDA:
                coluna -= i;
                break;
            case CIMA:
                linha -= i;
            case INVALIDA:
                break;
        }
        
        _VIDEO_Textura * ptr = _Flags_Objeto::texturaIdealUnitaria( getTipoObjeto() , _Flags_Objeto::INVISIVEL , i , direcao , ponto( linha , coluna ));
        adicionarCasa(  ptr , ponto( linha , coluna ) , vidasCasa );
    }

    printf("Criacao de CasasEmbarcacao realizada com sucesso.\n");
}

void Embarcacao::tornarVisivel( short indiceCasa ){
    if( not verificarIndiceCasa( indiceCasa ) ){
        throw _EXCECAO_STD_ForaDeIntervalo( "Excecao: Valor fora de intervalo em Embarcacao::tornarVisivel" ,
         ponto( 0 , getQuantidadeCasasOnline() - 1 ) , indiceCasa );
    }
    casa[ indiceCasa ]->setEstado( _Flags_Objeto::VISIVEL );
}

short Embarcacao::atualizarCasas(){
    short casasMortas = 0;

    //printf("Vou dar uma olhada nas vidas das casas.\n");
    for( int indice = 0 ; indice < getQuantidadeCasasOnline() ; indice++ ){
        // printf("Casa %d.\n", indice);
        // printf("Primeiro if.\n");
        if( casa[ indice ]->getVidas() == 0 ){
            //printf("A casa %d morreu.\n", indice);
            casa[ indice ]->setEstado( _Flags_Objeto::MORTO );
            //printf("Setei o estado dessa casa como morto.\n");
            casasMortas++;
        }
        else if( casa[ indice ]->getVidas() != _Flags_Objeto::vidasPadraoDesteTipo( getTipoObjeto() ) ){
            //printf("\n");
            casa[ indice ]->setEstado( _Flags_Objeto::VISIVEL );
            //printf("Settei o estado desta casa como visivel.\n");
        }
        //printf("Passei dos ifs.\n");
    }
    return casasMortas;
}

void Embarcacao::atualizarTexturasEmbarcacao(){
    for( int i = 0 ; i < getQuantidadeCasasOnline() ; i++ ){
        printf("Atualizando textura da casa %d\n", i);
        casa[ i ]->setTextura( 
        _Flags_Objeto::texturaIdealUnitaria( getTipoObjeto(), casa[i]->getEstado(), i, direcao,casa[i]->getPosicaoMatriz() ) );
    }
}

void Embarcacao::destruirCasasEmbarcacao(){
    for( int i = 0 ; i < getQuantidadeCasasOnline() ; i++ ){
        printf("Deletando a casa %d de uma embarcacao.\n", i);
        delete casa[i];
    }
}

//METODOS PUBLICOS

Embarcacao::Embarcacao( int8_t jogador , ponto posicao0 , Orientacao direcao ,
 uint8_t quantidadeCasas , uint8_t vidasCasa , int8_t tipoObjeto ) : Objeto( jogador , tipoObjeto ) ,
 posicao0( posicao0 ) , direcao(direcao){
    printf("Construtor Embarcacao() iniciado.\n");

    setEstadoVida( true );
    construirCasas( quantidadeCasas , vidasCasa );

    switch( getJogador() ){
        case JOGADOR_1:
            embarcacoesJogador1++;
            break;
        case JOGADOR_2:
            embarcacoesJogador2++;
            break;
    }

    printf("Construtor Embarcacao() realizado com sucesso.\n");
}

Embarcacao::~Embarcacao(){
    printf("Iniciando destrutor ~Embarcacao().\n");

    atualizarEmbarcacao();
    
    printf( "Atualizei as embarcacoes.\n");
    if( embarcacaoEstaViva() )
        setEstadoVida( _Flags_Objeto::MORTO );
    
    printf("\nDestruindo CasasEmbarcacao.\n");
    destruirCasasEmbarcacao();
    
    switch( getJogador() ){
        case JOGADOR_1:
            embarcacoesJogador1--;
            break;
        case JOGADOR_2:
            embarcacoesJogador2--;
            break;
    }

    printf("Destrutor ~Embarcacao() realizado com sucesso.\n");
}

short Embarcacao::getEmbarcacoesJogador1(){
    return embarcacoesJogador1;
}
short Embarcacao::getEmbarcacoesJogador2(){
    return embarcacoesJogador2;
}

short Embarcacao::getEmbarcacoesVivasJogador1(){
    return embarcacoesVivasJogador1;
}

short Embarcacao::getEmbarcacoesVivasJogador2(){
    return embarcacoesJogador2;
}

CasaEmbarcacao * Embarcacao::getCasa( short indiceCasa )const{
    if( not verificarIndiceCasa( indiceCasa ) ){
        throw _EXCECAO_STD_ForaDeIntervalo( "Excecao: Valor fora de intervalo em Embarcacao::getCasa." ,
         ponto( 0 , getQuantidadeCasasOnline() - 1 ) , indiceCasa );
    }

    return casa[ indiceCasa ];
}

// CasaEmbarcacao * Embarcacao::getCasa( short indiceCasa , bool flag ){
//     if( not verificarIndiceCasa( indiceCasa ) ){
//         throw _EXCECAO_STD_ForaDeIntervalo( "Excecao: Valor fora de intervalo em Embarcacao::getCasa." ,
//          ponto( 0 , getQuantidadeCasasOnline() - 1 ) , indiceCasa );
//     }
//     return casa[ indiceCasa ];
// }

void Embarcacao::adicionarCasa( _VIDEO_Textura * textura , ponto posicaoMatriz , short vidasCasa ){
    CasaEmbarcacao * ptr = new CasaEmbarcacao( textura , posicaoMatriz , vidasCasa );
    casa.push_back( ptr );
}


void Embarcacao::removerCasa(){
    this->casa.pop_back();
}

short Embarcacao::getQuantidadeCasasOnline()const{
    return  (short) casa.size();
}

// CasaEmbarcacao Embarcacao::operator()( short indiceCasa )const{
//     if( not verificarIndiceCasa( indiceCasa ) ){
//         throw _EXCECAO_STD_ForaDeIntervalo( "Excecao: Valor fora de intervalo em Embarcacao::operator()." ,
//          ponto( 0 , getQuantidadeCasasOnline() - 1 ) , indiceCasa );
//     }

//     return *casa[ indiceCasa ];
// }

ResultadoAcao * Embarcacao::tornarVisivel( ponto posicaoMatriz ){
    //A partir desta pilha sera lancada uma excecao caso a posicaoMatriz seja invalida
    tornarVisivel( procurarCasaCorrespondente( posicaoMatriz ) );
    atualizarTexturasEmbarcacao();
    return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
}

ResultadoAcao * Embarcacao::sofrerAtaque( ponto posicaoMatriz , uint8_t dano ){
    if( embarcacaoEstaViva() ){
        short indiceCasa = procurarCasaCorrespondente( posicaoMatriz );
        casa[ indiceCasa ]->deduzirVidas( dano );

        printf("Ataque na posicao (%hd,%hd) , corresponde a casa %hd.\n", posicaoMatriz.first, posicaoMatriz.second, indiceCasa);
        if( dano == 0 ){
            if( casa[ indiceCasa ]->getEstado() != _Flags_Objeto::MORTO )
                tornarVisivel( indiceCasa );
                
            return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );
        }
        else{
            atualizarCasas();
            atualizarTexturasEmbarcacao();
            return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
        }
    }
    else
        //Se o objeto estiver morto por padrao o resultado da acao sera neutralizado
        return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );
}

// ResultadoAcao * Embarcacao::curar( ponto posicaoMatriz , uint8_t cura ){
//     //Notar que se essa posicao de matriz nao pertencer a esta embarcacao, o indice sera -1
//     //ao tentar curar a casa de indice -1, o vector lancara uma excecao out_of_range
//     short indice = procurarCasaCorrespondente( posicaoMatriz );
    
//     if( casa[ indice ]->getVidas() + cura > _Flags_Objeto::vidasPadraoDesteTipo( getTipoObjeto() ) )
//         cura = _Flags_Objeto::vidasPadraoDesteTipo( getTipoObjeto() ) - casa[ indice ]->getVidas();
    
//     casa[ indice ]->acrescentarVidas( cura );

//     if( cura == 0 )
//         return new ResultadoAcao( getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );
//     else
//         return new ResultadoAcao( getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
// }

ResultadoAcao * Embarcacao::exibirNaTela( ponto posicaoMatriz ){
    short indiceCasa = procurarCasaCorrespondente( posicaoMatriz );
    if( indiceCasa == -1 )
        return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );

    if( Objeto::printarBloco( casa[ indiceCasa ]->getTextura() ) )
        return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
    else
        return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );
}

void Embarcacao::atualizarObjeto(){
    atualizarEmbarcacao();
}

bool Embarcacao::embarcacaoEstaViva(){
    //atualizarEstadoVida();
    return estadoVida;
}

void Embarcacao::atualizarEmbarcacao(){
    if( embarcacaoEstaViva() ){
        //printf("Vou ver quantas casas mortas tem.\n");

        short casasMortas = atualizarCasas();

        //printf("Atualizei casas.\n");
        if( casasMortas == getQuantidadeCasasOnline() ){
           setEstadoVida( false );
           printf("EMBARCACAO ABATIDA\n");
           atualizarTexturasEmbarcacao();
           //printf("Atualizei texturas da embarcacao.\n");
        } 
    }
    return;
}