#include "BatalhaNaval.hpp"

#include <iostream>
#include <cstdio>
#include <string>

using namespace std;

int main(){
	_SERVICE_VIDEO_ProcessamentoVideo * video = new _SERVICE_VIDEO_ProcessamentoVideo( "Batalha Naval" , _Flags_Video::LARGURA_TELA ,
	_Flags_Video::ALTURA_TELA );

	SDL_Init( SDL_INIT_EVENTS );

	_Flags_Objeto::iniciarTexturas( video->getRenderer() );

	srand( time(0) );

	vector<string> arquivos;
	arquivos.push_back("doc/menu_principal.png");
	arquivos.push_back("doc/selecionar_modo.png");
	arquivos.push_back("doc/escolha_mapa.png");

	_SERVICE_Menu * menu = new _SERVICE_Menu( video->getRenderer() , arquivos );

	SDL_Event event;
	bool runGlobal = true;

	string mapa = "";

	while( runGlobal ){
		menu->reiniciarMenu();

		while( mapa == "" && runGlobal ){
			while( SDL_PollEvent( &event ) != 0 ){
				if( event.type == SDL_MOUSEBUTTONDOWN ){
					if( event.button.button == SDL_BUTTON_LEFT ){
						int8_t continuarMenu = menu->inputClick( ponto( event.button.x , event.button.y ) );
						if( continuarMenu == 1 ){
							switch( menu->mapaDeJogoEscolhido() ){
								case 1:
									mapa = "doc/map_1.txt";
									break;
								case 2:
									mapa = "doc/map_2.txt";
									break;
								case 3:
									mapa = "doc/map_3.txt";
									break;
								case -1:
									mapa = "nan";
									runGlobal = false;
									break;
							}
						}
					}
				}
				else if( event.type == SDL_QUIT ){
					runGlobal = false;
					mapa = "nan";
				}
			}
			video->limparTela();
			video->inserirTexturaNaTela( menu->getTextura() );
			video->exibirTela();

			SDL_Delay(100);
		}
		if( mapa == "nan" )
			break;

		Jogador * jogador1 = new Jogador( JOGADOR_1 , mapa );
		Jogador * jogador2 = new Jogador( JOGADOR_2 , mapa );

		int8_t vez = JOGADOR_1;

		while( Embarcacao::getEmbarcacoesVivasJogador1() != 0 && Embarcacao::getEmbarcacoesJogador2() != 0 && runGlobal ){
			if( vez == JOGADOR_1 ){
				jogador1->atualizarJogador();
				bool atacou = false;
				while( not atacou && runGlobal ){
					short x = -1, y = -1;
					while( SDL_PollEvent( &event ) ){
						if( event.type == SDL_MOUSEBUTTONDOWN ){
							if( event.button.button == SDL_BUTTON_LEFT ){
								x = event.button.x;
								y = event.button.y;
							}
						}
						else if( event.type == SDL_QUIT )
							runGlobal = false;
					}
					if( not runGlobal )
						break;

					video->limparTela();
					video->exibirBackGround();
					
					if( x >= 0 && y >= 0 ){
						ponto posicaoTabuleiro = _Flags_Video::posicaoTabuleiroDestePixel( ponto(x,y) );

						if( posicaoTabuleiro.first >= 0 && posicaoTabuleiro.first < 13 && posicaoTabuleiro.second >= 0 &&
						posicaoTabuleiro.second < 13 ){
							ResultadoAcao * acaoAtaque = jogador1->atacarPosicaoTabuleiro_unico( ponto(posicaoTabuleiro.first,posicaoTabuleiro.second) );
							video->exibirFeedBackJogador( acaoAtaque );
							delete acaoAtaque;
							atacou = true;
						}
					}
					jogador1->inserirTabuleiroNaTela();
					video->exibirTela();

					if(atacou)
						SDL_Delay(2000);

					SDL_Delay(100);
				}
				vez = JOGADOR_2;
			}
			else if( vez == JOGADOR_2 && Embarcacao::getEmbarcacoesVivasJogador1() != 0 && Embarcacao::getEmbarcacoesJogador2() != 0 && runGlobal ){
				bool atacou = false;
				while( not atacou && runGlobal ){
					jogador2->atualizarJogador();
					short x = -1, y = -1;
					while( SDL_PollEvent( &event ) ){
						if( event.type == SDL_MOUSEBUTTONDOWN ){
							if( event.button.button == SDL_BUTTON_LEFT ){
								x = event.button.x;
								y = event.button.y;
							}
						}
						else if( event.type == SDL_QUIT )
							runGlobal = false;
					}
					if( not runGlobal )
						break;

					video->limparTela();
					video->exibirBackGround();
					
					if( x >= 0 && y >= 0 ){
						ponto posicaoTabuleiro = _Flags_Video::posicaoTabuleiroDestePixel( ponto(x,y) );

						if( posicaoTabuleiro.first >= 0 && posicaoTabuleiro.first < 13 && posicaoTabuleiro.second >= 0 &&
						posicaoTabuleiro.second < 13 ){
							ResultadoAcao * acaoAtaque = jogador2->atacarPosicaoTabuleiro_unico( ponto(posicaoTabuleiro.first,posicaoTabuleiro.second) );
							video->exibirFeedBackJogador( acaoAtaque );
							delete acaoAtaque;
							atacou = true;
						}
					}
					jogador2->inserirTabuleiroNaTela();
					video->exibirTela();

					if( atacou )
						SDL_Delay(2000);

					SDL_Delay(100);
				}
				vez = JOGADOR_1;
			}
		}
		
		delete jogador1;
		delete jogador2;
	}
	
	_Flags_Objeto::destruirTexturas();
	delete video;
	return 0;
}