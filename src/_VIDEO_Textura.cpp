#include "_VIDEO_Textura.hpp"
#include "_EXCECAO_GRAPHICS_Video.hpp"
#include "_EXCECAO_STD_ForaDeIntervalo.hpp"
#include "_Flags_Video.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "ponto.h"
#include "Orientacao.hpp"

#include <cstdio>
#include <string>

void _VIDEO_Textura::criarTextura( SDL_Renderer * renderer , const string & caminhoImagem , bool imagemCompleta ){
    SDL_Surface * superficieTemporaria = IMG_Load( caminhoImagem.c_str() );

    if( superficieTemporaria == NULL ){
        throw _EXCECAO_GRAPHICS_Video( IMG_GetError() );
    }

    textura = SDL_CreateTextureFromSurface( renderer , superficieTemporaria );

    if( textura == NULL ){
        printf("Erro ao criar uma nova SDL_Texture em _VIDEO_Textura::criarTextura.\n");
        throw _EXCECAO_GRAPHICS_Video( IMG_GetError() );
    }

    if( imagemCompleta ){
        recorteTextura.x = 0;
        recorteTextura.y = 0;
        recorteTextura.w = superficieTemporaria->w;
        recorteTextura.h = superficieTemporaria->h;

        areaTela.x = 0;
        areaTela.y = 0;
        areaTela.w = superficieTemporaria->w;
        areaTela.h = superficieTemporaria->h;
    }

    SDL_FreeSurface( superficieTemporaria );
}

void _VIDEO_Textura::setRetangulos( SDL_Rect * areaTela , SDL_Rect * recorteTextura ){
    this->areaTela = *areaTela;
    this->recorteTextura = *recorteTextura;
}

void _VIDEO_Textura::verificarTextura(){
    if( not _Flags_Video::isRecorteValido( &recorteTextura ) || not _Flags_Video::isAreaValida( &areaTela ) ){
        throw _EXCECAO_GRAPHICS_Video( "Area de recorte ou area de exibicao de textura invalida." );
    }
}

void _VIDEO_Textura::destruirTextura(){
    if( not isAlias ){
        //printf("Vou tentar usar SDL_DestroyTexture.\n");
        SDL_DestroyTexture( textura );
        textura = NULL;
    }
    //printf("Textura destruida com sucesso em _VIDEO_Textura::destruirTextura.\n");
}

_VIDEO_Textura::_VIDEO_Textura( SDL_Renderer * renderer , const string & caminhoImagem ){
    printf("Construtor para textura full iniciado.\n");

    isAlias = false;
    criarTextura( renderer , caminhoImagem , true );
    verificarTextura();

    printf("Construtor para textura full finalizado com sucesso.\n");
}

_VIDEO_Textura::_VIDEO_Textura( SDL_Renderer * renderer , const string & caminhoImagem , SDL_Rect areaTela ,
SDL_Rect recorteTextura ){
    printf("Construtor para textura recortada iniciado.\n");

    isAlias = false;
    criarTextura( renderer , caminhoImagem );
    setRetangulos( &areaTela , &recorteTextura );
    verificarTextura();

    printf("Construtor para textura recortada finalizado com sucesso\n");
}

_VIDEO_Textura::_VIDEO_Textura( SDL_Texture * texturaBase , ponto coordenadasTextura , ponto coordenadasTela ){
    printf("Construtor para textura alias iniciado.\n");
    isAlias = true;
    textura = texturaBase;

    this->recorteTextura.y = coordenadasTextura.first;
    this->recorteTextura.x = coordenadasTextura.second;
    printf("x da text = %d.\ny da text = %d\n", recorteTextura.x, recorteTextura.y);

    this->recorteTextura.w = _Flags_Video::wImg;
    this->recorteTextura.h = _Flags_Video::hImg;

    this->areaTela.x = coordenadasTela.first;
    this->areaTela.y = coordenadasTela.second;

    this->areaTela.w = _Flags_Video::wTel;
    this->areaTela.h = _Flags_Video::hTel;

    verificarTextura();

    printf("Construtor para textura alias finalizado com sucesso.\n");
}

_VIDEO_Textura::_VIDEO_Textura( SDL_Renderer * renderer , const string & caminhoImagem , SDL_Rect areaTela ){
    isAlias = false;
    criarTextura( renderer , caminhoImagem , true );
    this->areaTela = areaTela;
    verificarTextura();

    printf("Construtor _VIDEO_Textura executado com sucesso.\n");
}

_VIDEO_Textura::~_VIDEO_Textura(){
        destruirTextura();
        printf("Destrutor ~_VIDEO_Textura() executado com sucesso.\n");
}

SDL_Rect * _VIDEO_Textura::getAreaTela(){
    return &areaTela;
}

SDL_Rect * _VIDEO_Textura::getRecorteTextura(){
    return &recorteTextura;
}

_VIDEO_Textura * _VIDEO_Textura::getParte( bool estaVivo , int8_t indice , Orientacao direcao , ponto posicaoMatriz ){
    _VIDEO_Textura * objTemp = NULL;
    ponto pixelTabuleiro = _Flags_Video::pixelDestaPosicaoTabuleiro( posicaoMatriz );

    switch ( direcao ){
        case ESQUERDA:
        {
            if( estaVivo ){
                if( areaTela.w > 270 && areaTela.w < 290 ){
                    objTemp = new _VIDEO_Textura( textura , ponto( 0 , (3-indice) * _Flags_Video::wImg  ), pixelTabuleiro );
                    printf("\nCriando textura para porta_aviao esquerdo.\n\n");
                }
                else if( areaTela.w > 135 && areaTela.w < 145  ){
                    objTemp = new _VIDEO_Textura( textura , ponto( 0 , (1-indice) * _Flags_Video::wImg ) , pixelTabuleiro);
                    printf("\nCriando textura para casa %hhd, sub esquerdo, %hhd carregada\n\n", indice, 1-indice);
                }
                else if( areaTela.w > 65 && areaTela.w < 75 ){
                    objTemp = new _VIDEO_Textura( textura , ponto(0,0) , pixelTabuleiro);
                    printf("\nCriando textura para canoa esquero\n\n");
                }
            }
            else{
                if( areaTela.w > 270 && areaTela.w < 290 ){
                    objTemp = new _VIDEO_Textura( textura , ponto( 3 * _Flags_Video::hImg , (3-indice) * _Flags_Video::wImg  ), pixelTabuleiro );
                    printf("\nCriando textura para porta_aviao morto esquerdo.\n");
                }
                else if( areaTela.w > 135 && areaTela.w < 145  ){
                    objTemp = new _VIDEO_Textura( textura , ponto( 3 * _Flags_Video::hImg , (1-indice) * _Flags_Video::wImg ) , pixelTabuleiro);
                    printf("\nCriando textura para sub morto esquerdo.\n\n");
                }
                else if( areaTela.w > 65 && areaTela.w < 75 ){
                    objTemp = new _VIDEO_Textura( textura , ponto( 3 * _Flags_Video::hImg  , 0 ) , pixelTabuleiro);
                    printf("\nCriando textura para canoa morta esquerda\n\n");
                }
            }
            break;
        }
        case DIREITA:
        {
            if( estaVivo ){
                objTemp = new _VIDEO_Textura( textura , ponto( 0 , indice * _Flags_Video::wImg ) , pixelTabuleiro );
            }
            else{
                objTemp = new _VIDEO_Textura( textura , ponto( 3 * _Flags_Video::hImg , indice * _Flags_Video::wImg )
                ,pixelTabuleiro );
            }
            break;
        }
        case CIMA:{
            if( estaVivo ){
                objTemp = new _VIDEO_Textura( textura , ponto( 2 * _Flags_Video::hImg , indice * _Flags_Video::wImg ) 
                ,pixelTabuleiro );
            }
            else{
                objTemp = new _VIDEO_Textura( textura , ponto( 5 * _Flags_Video::hImg , indice * _Flags_Video::wImg ) 
                ,pixelTabuleiro );
            }
            break;
        }
        case BAIXO:{
            if( estaVivo ){
                objTemp = new _VIDEO_Textura( textura , ponto( _Flags_Video::hImg , indice * _Flags_Video::wImg )
                ,pixelTabuleiro );
            }
            else{
                objTemp = new _VIDEO_Textura( textura , ponto( 4 * _Flags_Video::hImg , indice * _Flags_Video::wImg )
                ,pixelTabuleiro );
            }
            break;
        }
    }
    printf("Retornando objTemp.\n");
    return objTemp;
}

SDL_Texture * _VIDEO_Textura::getTextura()const{
    return textura;
}