#include "Submarino.hpp"
#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "Orientacao.hpp"
#include "ponto.h"

#include <cstdlib>

using namespace std;

short Submarino::submarinosJogador1 = 0;
short Submarino::submarinosJogador2 = 0;

//CODIGO NERFADO
// bool Submarino::habilidadeEspecial( ponto posicaoMatriz ){
//     bool habilidadeEspecialFlag = false;
//     short indice = procurarCasaCorrespondente( posicaoMatriz );

//     if( indice == 0 && ataquesFataisSofridos.first < 2 && getCasa( indice )->getEstado( ) == _Flags_Objeto::MORTO ){
//         ataquesFataisSofridos.first++;
//         curar( posicaoMatriz , _Flags_Objeto::VIDAS_SUBMARINO );
//         habilidadeEspecialFlag = true;
//     }
//     else if( indice == 1 && ataquesFataisSofridos.second < 2 && getCasa( indice )->getEstado( ) == _Flags_Objeto::MORTO ){
//         ataquesFataisSofridos.second++;
//         curar( posicaoMatriz , _Flags_Objeto::VIDAS_SUBMARINO );
//         habilidadeEspecialFlag = true;
//     }

//     return habilidadeEspecialFlag;
// }

Submarino::Submarino( int8_t jogador , ponto posicao0 , Orientacao direcao )
:Embarcacao( jogador , posicao0 , direcao , _Flags_Objeto::CASAS_SUBMARINO , _Flags_Objeto::VIDAS_SUBMARINO ,
 _Flags_Objeto::SUBMARINO ){

    switch( jogador ){
        case JOGADOR_1:
            submarinosJogador1++;
            break;
        case JOGADOR_2:
            submarinosJogador2++;
            break;
    }
}

Submarino::~Submarino(){
    switch( getJogador() ){
        case JOGADOR_1:
            submarinosJogador1--;
            break;
        case JOGADOR_2:
            submarinosJogador2--;
            break;
    }
}

short Submarino::getSubmarinosJogador1(){
    return submarinosJogador1;
}
short Submarino::getSubmarinosJogador2(){
    return submarinosJogador2;
}

ResultadoAcao * Submarino::sofrerAtaque( ponto posicaoMatriz , uint8_t dano ){
    printf("\n\nSubmarino sofrendo ataque.\n");
    return Embarcacao::sofrerAtaque( posicaoMatriz , dano );
}

