#include "ResultadoAcao.hpp"
#include <cstdint>

ResultadoAcao::ResultadoAcao( int8_t tipoObjeto , int8_t resultado ){
    this->tipoObjeto = tipoObjeto;
    this->resultado  =  resultado;
}

int8_t ResultadoAcao::getTipoObjeto(){
    return tipoObjeto;
}

int8_t ResultadoAcao::getResultado(){
    return resultado;
}