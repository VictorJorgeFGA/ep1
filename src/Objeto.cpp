#include "Objeto.hpp"

#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "_SERVICE_VIDEO_ProcessamentoVideo.hpp"
#include "_VIDEO_Textura.hpp"

#include "ponto.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

short Objeto::objetosJogador1 = 0;
short Objeto::objetosJogador2 = 0;

//METODOS UTILITARIOS PROTECTED
void Objeto::printarBloco( int8_t estadoObjeto )const{
    if( estadoObjeto == _Flags_Objeto::INVISIVEL )
        printf("\033[0;%hhdm\u2588\u2588 \033[0;0m", _Flags_Objeto::AGUA);

    else if( estadoObjeto == _Flags_Objeto::VISIVEL || estadoObjeto == _Flags_Objeto::MORTO ){
        printf("\033[0;%hhdm\u2588\u2588 \033[0;0m", getTipoObjeto());
    }
    else
    {
        printf("\nEstado invalido fornecido em printarBloco: %hhd.\n", estadoObjeto );
        exit(1);
    }
}

bool Objeto::printarBloco( _VIDEO_Textura * textura ){
    _SERVICE_VIDEO_ProcessamentoVideo * video = new _SERVICE_VIDEO_ProcessamentoVideo();
    bool sucessFlag = video->inserirTexturaNaTela( textura );
    delete video;
    return sucessFlag;
}

void Objeto::setTipoObjeto( int8_t tipoObjeto ){
    this->tipoObjeto = tipoObjeto;
}

Objeto::Objeto( int8_t jogador , int8_t tipoObjeto ) : jogador(jogador) , tipoObjeto(tipoObjeto){
    switch( getJogador() ){
        case JOGADOR_1:
            objetosJogador1++;
            break;
        case JOGADOR_2:
            objetosJogador2++;
            break;
    }

    if( getObjetosOnline() > _Flags_Objeto::QNTD_MAX_OBJETOS ){
        printf("\nErro! Quantidade maxima de objetos excedida:  %d\n", getObjetosOnline());
        exit(1);
    }
}

Objeto::~Objeto(){
    switch( getJogador() ){
        case JOGADOR_1:
            objetosJogador1--;
            break;
        case JOGADOR_2:
            objetosJogador2--;
            break;
    }
}

short Objeto::getObjetosOnline(){
    return getObjetosJogador1() + getObjetosJogador2();
}

short Objeto::getObjetosJogador1(){
    return objetosJogador1;
}

short Objeto::getObjetosJogador2(){
    return objetosJogador2;
}

int8_t Objeto::getJogador()const{
    return jogador;
}

void Objeto::setJogador( short jogador ){
    this->jogador = jogador;
}

int8_t Objeto::getTipoObjeto()const{
    return tipoObjeto;
}

void Objeto::atualizarObjeto(){
    return;
}