#include "_Flags_Video.hpp"
#include "ponto.h"

#include <cstdint>
#include <SDL2/SDL.h>

const int16_t _Flags_Video::wImg = 70; //Largura padrao em pixels de um bloco de imagem
const int16_t _Flags_Video::hImg = 70; //Altura padrao em pixels de um bloco de imagem

const int16_t _Flags_Video::wTel = 65;
const int16_t _Flags_Video::hTel = 65;

bool _Flags_Video::isRecorteValido( SDL_Rect * retangulo ){
    if( retangulo->x < 0 || retangulo->y < 0 || retangulo->x > LARGURA_TELA || retangulo->y > ALTURA_TELA
    ||  retangulo->w <= 0|| retangulo->h <= 0|| retangulo->w > LARGURA_TELA || retangulo->h > ALTURA_TELA )
        return false;

    return true;
}

bool _Flags_Video::isAreaValida( SDL_Rect * retangulo ){
    if( retangulo->x < 0 || retangulo->y < 0 || retangulo->x > LARGURA_TELA || retangulo->y > ALTURA_TELA
    ||  retangulo->w <= 0|| retangulo->h <= 0|| retangulo->w > LARGURA_TELA || retangulo->h > ALTURA_TELA )
        return false;
    
    return true;
}

ponto _Flags_Video::pixelDestaPosicaoTabuleiro( ponto posicaoTabuleiro ){
    short x = BORDA_ESQUERDA + posicaoTabuleiro.second  * wTel;
    short y = BORDA_SUPERIOR + posicaoTabuleiro.first * hTel;
    
    return ponto( x , y );
}

ponto _Flags_Video::posicaoTabuleiroDestePixel( ponto pixel ){
    // i * wTel + BORDA_ESQUERDA = xpixel
    // i * wTel = xpixel - BORDA_ESQUERDA;
    // i = ( xpixel - BORDA_ESQUERDA )/wTel
    short coluna = ( pixel.first - _Flags_Video::BORDA_ESQUERDA ) / _Flags_Video::wTel;
    short linha = ( pixel.second - _Flags_Video::BORDA_SUPERIOR ) / _Flags_Video::hTel;
    printf("Clique no pixel %hd,%hd, refere-se a posicao %hd,%hd.\n", pixel.first, pixel.second, linha,coluna);
    return ponto( linha , coluna );
}