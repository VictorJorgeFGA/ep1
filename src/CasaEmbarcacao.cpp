#include "CasaEmbarcacao.hpp"
#include "Objeto.hpp"
#include "ponto.h"

#include "_Flags_Objeto.hpp"
#include "_VIDEO_Textura.hpp"

#include <stdio.h>
#include <cstdlib>

using namespace std;

void CasaEmbarcacao::setPosicaoMatriz( ponto posicaoMatriz ){
    this->posicaoMatriz = posicaoMatriz;
}

CasaEmbarcacao::CasaEmbarcacao( _VIDEO_Textura * textura , ponto posicaoMatriz , uint8_t vidas )
:posicaoMatriz( posicaoMatriz ), vidas( vidas ), estado( _Flags_Objeto::INVISIVEL ){
    this->textura = textura;

    printf("Construtor CasaEmbarcacao executado com sucesso.\n");
}

void CasaEmbarcacao::setVidas( uint8_t vidas ){
    if( vidas < 0 ){
        printf("\nErro! Quantidade de vidas invalida fornecida. %d\n", vidas);
        return;
    }
    this->vidas = vidas;
}

CasaEmbarcacao::~CasaEmbarcacao(){
    printf("Destrutor CasaEmbarcacao iniciado.\n");
    if( textura != NULL ){
        delete textura;
        textura = NULL;
    }
    printf("Destrutor CasaEmbarcacao executado com sucesso.\n");
}

ponto CasaEmbarcacao::getPosicaoMatriz()const{
    return posicaoMatriz;
}

uint8_t CasaEmbarcacao::getVidas()const{
    //printf("Retornando quantas vidas.\n");
    return vidas;
}

uint8_t CasaEmbarcacao::getEstado()const{
    return estado;
}

void CasaEmbarcacao::setEstado( uint8_t estado ){
    if( estado != _Flags_Objeto::VISIVEL && estado != _Flags_Objeto::INVISIVEL && estado != _Flags_Objeto::MORTO ){
        printf("\nErro crítico! Estado invalido fornecido em CasaEmbarcacao::setEstado: %hhd\n", estado);
        exit(1);
    }
    
    this->estado = estado;
}

_VIDEO_Textura * CasaEmbarcacao::getTextura(){
    return textura;
}

void CasaEmbarcacao::setTextura( _VIDEO_Textura * textura ){
    if( this->textura != NULL ){
        printf("Deletando a textura antiga.\n");
        delete this->textura;
    }
    this->textura = textura;
}

void CasaEmbarcacao::deduzirVidas( uint8_t dano ){
    int8_t novaVidas = getVidas() - dano;
    if( novaVidas < 0 )
        novaVidas = 0;

    setVidas( novaVidas );
}

void CasaEmbarcacao::acrescentarVidas( uint8_t cura ){
    setVidas( getVidas() + cura );
}