#include "_SERVICE_ARQ_ProcessamentoMapa.hpp"
#include "_SERVICE_ARQ_DadosFormatados.hpp"

#include "_EXCECAO_ARQ_Invalido.hpp"

#include "Orientacao.hpp"
#include "_Flags_Jogador.hpp"
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;


//_SERVICE_ARQ_ProcessamentoMapa _SERVICE_ARQ_ProcessamentoMapa_OBJ_ERRO( "doc/_erro.txt" , COMPUTER );

void _SERVICE_ARQ_ProcessamentoMapa::setValidade( bool possuiEstadoValido ){
    this->possuiEstadoValido = possuiEstadoValido;
}

long _SERVICE_ARQ_ProcessamentoMapa::procurarBlocoDoJogadorNoArquivo( short jogadorID ){
    long posicaoNoArquivo = -1;
    string linhaTeste;

    arquivoMapa.seekg(0);

    while( getline( arquivoMapa , linhaTeste )  ){
        if( linhaTeste == "# player_1" and jogadorID == JOGADOR_1 ){
            posicaoNoArquivo = arquivoMapa.tellg();
            break;
        }
        else if( linhaTeste == "# player_2" and jogadorID == JOGADOR_2 ){
            posicaoNoArquivo = arquivoMapa.tellg();
            break;
        }
        else if( linhaTeste == "# computer" and jogadorID == COMPUTER ){
            posicaoNoArquivo = arquivoMapa.tellg();
            break;
        }
    }

    return posicaoNoArquivo;
}

bool _SERVICE_ARQ_ProcessamentoMapa::verificarIndice( short indice )const{
    if( indice < 0 || indice >= ( (short) informacao.size() ) )
        return false;
    else
        return true;
}

bool _SERVICE_ARQ_ProcessamentoMapa::verificarJogadorID( short jogadorID ){
    bool sucessFlag = false;

    switch ( jogadorID ){
        case JOGADOR_1:
        case JOGADOR_2:
        case COMPUTER:
            sucessFlag = true;
            break;
    }

    return sucessFlag;
}

void _SERVICE_ARQ_ProcessamentoMapa::extrairDados( long posicaoInicial ){
    arquivoMapa.seekg( posicaoInicial );
    short i,
          j;
    
    string tipo,
           direcao;
    /*O laco eh interrompido quando a linha a ser processada nao possibilitar a extracao
    na ordem ( short short string string ). Ou seja, quando encontrar o marcador # player_n
    ou quando atingir EOF o laco sera interrompido*/
    for( int iteracao = 0 ; arquivoMapa >> i >> j >> tipo >> direcao ; iteracao++ ){
        informacao.push_back( _SERVICE_ARQ_DadosFormatados( i , j , tipo , direcao ) );

        if( not informacao[ iteracao ].dadosSaoValidos() ){
            setValidade( false );
            break;
        }
    }
}

_SERVICE_ARQ_ProcessamentoMapa::_SERVICE_ARQ_ProcessamentoMapa( const string &caminhoArquivo ,
short jogadorID) : possuiEstadoValido( true ) {

    if( not verificarJogadorID( jogadorID ) ){
        printf("\nErro. ID de jogador invalida fornecida em _SERVICE_ARQ_ProcessamentoMapa::_SERVICE_ARQ_ProcessamentoMapa: %hd.\n", jogadorID);

        setValidade( false );
    }
    else{
        arquivoMapa.open( caminhoArquivo , ios::in );

        if( !arquivoMapa ){
            printf("\nErro ao abrir o arquivo ");
            cout << caminhoArquivo;
            printf(" em construtor _SERVICE_ARQ_ProcessamentoMapa::_SERVICE_ARQ_ProcessamentoMapa\n");
            
            setValidade( false );
        }
        else{
            extrairDados( procurarBlocoDoJogadorNoArquivo( jogadorID ) );
        }
    }
}

// _SERVICE_ARQ_ProcessamentoMapa::_SERVICE_ARQ_ProcessamentoMapa( const _SERVICE_ARQ_ProcessamentoMapa 
// &objeto_processamentoMapa ){
    
// }

_SERVICE_ARQ_ProcessamentoMapa::~_SERVICE_ARQ_ProcessamentoMapa(){
    arquivoMapa.close();
}

_SERVICE_ARQ_DadosFormatados _SERVICE_ARQ_ProcessamentoMapa::getInformacao( short indice )const{
    if( not verificarIndice( indice ) ){
        printf("\nErro! Indice invalido fornecido em _SERVICE_ARQ_ProcessamentoMapa::getInformacao: %hd.\n", indice);
        exit(1);
    }

    return informacao[ indice ];
}

bool _SERVICE_ARQ_ProcessamentoMapa::operator!()const{
    return not possuiEstadoValido;
}

short _SERVICE_ARQ_ProcessamentoMapa::getQuantidadeInformacoes()const{
    return informacao.size();
}

bool _SERVICE_ARQ_ProcessamentoMapa::operator==(const _SERVICE_ARQ_ProcessamentoMapa & objeto_processamentoMapa ){
    if( ! ( getQuantidadeInformacoes() == objeto_processamentoMapa.getQuantidadeInformacoes() ) )
        return false;

    else{

        bool sucessFlag = true;

        for( short indice = 0 ; indice < getQuantidadeInformacoes() ; indice++ ){
            if( ! ( informacao[ indice ] == objeto_processamentoMapa.getInformacao( indice ) ) ){
                sucessFlag = false;
                break;
            }
        }
        return sucessFlag;
    }
}