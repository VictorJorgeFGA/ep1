#include "Jogador.hpp"
#include "Tabuleiro.hpp"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "_Flags_Tabuleiro.hpp"

#include "_EXCECAO_STD_ForaDeIntervalo.hpp"

#include <string>
#include <vector>
#include <cstdlib>
#include <queue>
#include "ponto.h"
#include "ResultadoAcao.hpp"

using namespace std;

const uint8_t Jogador::PODER_DE_ATAQUE_PADRAO = 1;
//const uint8_t Jogador::DISTANCIA_PARA_ATAQUE_EM_AREA_PADRAO = 2;
//const uint8_t Jogador::DIVISOR_PODER_DE_ATAQUE = 2;
//const uint8_t Jogador::DIVISOR_DISTANCIA_PARA_ATAQUE_EM_AREA = 5;

//CODIGO NERFADO POR QUESTOES DE PRAZO
// uint8_t Jogador::deduzirPotenciaDano( ponto pontoAtual , ponto pontoOrigem ){
//     short distancia = -1;

//     if( ( distancia = abs( pontoAtual.first - pontoOrigem.first ) ) >= abs( pontoAtual.second - pontoOrigem.second ) ){}
//     else
//         distancia  = abs( pontoAtual.second - pontoOrigem.second );

//     //Dano cai pela metade para cada casa de distancia da origem
//     return getPoderDeAtaque() / (( distancia * 2 ) + 2);     
// }

// vector<ResultadoAcao * > * Jogador::floodFill( ponto posicaoInicial , uint8_t potenciaAtaque ){
//     if( not mapa.estaDentroTabuleiro( posicaoInicial ) ){
//         return NULL;
//     }
//     //Cria o vetor de flags que sera retornado
//     vector<ResultadoAcao * > * listaObjetosAtacados = new vector<ResultadoAcao * >;

//     //Extracao de linhas e colunas
//     short m = mapa.getTamanho().first,
//           n = mapa.getTamanho().second;

//     //Cria uma matriz linearizada de flags que informa se a posicao ja foi atacada ou nao
//     bool * flagsMapa = new bool[ m * n ];

//     //Preenche todas as posicoes com falso a priori
//     for( int i = 0 ; i < m * n ; i++)
//         flagsMapa[i] = false;
    
//     //incrementos do floodfill
//     int8_t dx[] = { 1,  1,  1, -1, -1, -1,  0,  0};
//     int8_t dy[] = {-1,  0,  1, -1,  0,  1, -1,  1};

//     //Distancia maxima permitida de ataque para este jogador
//     short distanciaPermitida = getDistanciaParaAtaqueEmArea();

//     //Fila de posicoes para serem atacadas
//     queue<ponto> paraAtacar;
//     //Insere a posicao inicial na fila
//     paraAtacar.push( posicaoInicial );

//     //Enquanto houver posicoes para serem atacadas continuar o algoritmo
//     while( ! paraAtacar.empty() ){
//         //extrai uma posicao para ser atacada
//         ponto posicaoAtual = paraAtacar.front();
//         //remove a posicao extraida da fila
//         paraAtacar.pop();

//         //diferenciacao entre revelar e atacar
//         if( potenciaAtaque == 0 )
//             listaObjetosAtacados->push_back( revelarPosicaoTabuleiro_unico( posicaoAtual ) );
//         else
//             listaObjetosAtacados->push_back( atacarPosicaoTabuleiro_unico( posicaoAtual , deduzirPotenciaDano( posicaoAtual , posicaoInicial ) ) );
        
//         /* Se x recebesse o first o algoritmo manipularia o que intuitivamente seria horizontal
//         como vertical e vice-versa.*/
//         short x = posicaoAtual.second, // x recebe papel de coluna
//               y = posicaoAtual.first;  // y recebe papel de linha

//         //Diz que esta posicao ja foi atacada
//         flagsMapa[ x * n + y ] = true;

//         //Percorre todas as casas adjacentes em busca de uma nova posicao pra atacar
//         for( short i = 0 ; i < 8 ; i++ ){
//             //Se a posicao ja foi atacada continuar
//             if( flagsMapa[ ( x + dx[i] ) * n + y + dy[i] ] == true )
//                 continue;

//             //Se a posicao ainda esta dentro da area maxima para este jogador, jogar ela dentro da fila
//             else if( x + dx[i] <= posicaoInicial.first  + distanciaPermitida &&
//                      x + dx[i] >= posicaoInicial.first  - distanciaPermitida &&
//                      y + dy[i] <= posicaoInicial.second + distanciaPermitida &&
//                      y + dy[i] <= posicaoInicial.second - distanciaPermitida &&
//                      mapa.estaDentroTabuleiro( ponto( x + dx[i] , y + dy[i] ) ) )
//                 paraAtacar.push( ponto( x + dx[i] , y + dy[i] ) );
//         }
//     }
//     return listaObjetosAtacados;
// }

// void Jogador::atualizarPoderDeAtaque(){
//     this->poderDeAtaque = PODER_DE_ATAQUE_PADRAO + ( getPontuacao() / DIVISOR_PODER_DE_ATAQUE ); 
// }

//CODIGO NERFADO POR QUESTOES DE PRAZO
// void Jogador::atualizarDistanciaParaAtaqueEmArea(){
//     this->distanciaParaAtaqueEmArea = DISTANCIA_PARA_ATAQUE_EM_AREA_PADRAO + ( getPontuacao() / DIVISOR_DISTANCIA_PARA_ATAQUE_EM_AREA );
// }

// Jogador::Jogador( int8_t jogadorID , const string & nomeJogador )
// :jogadorID( jogadorID ) , pontuacao( 0 ) , nomeJogador( nomeJogador ) , mapa( jogadorID ){ 
//     atualizarJogador();
// }

Jogador::Jogador( int8_t jogadorID , const string & caminhoArquivoMapa ,
 const string & nomeJogador )
:jogadorID( jogadorID ), nomeJogador( nomeJogador ) ,
 mapa( jogadorID , caminhoArquivoMapa ){ 
     atualizarJogador();
}

string Jogador::getNomeJogador()const{
    return nomeJogador;
}

void Jogador::setNomeJogador( const string & nomeJogador ){
    this->nomeJogador = nomeJogador;
}

int8_t Jogador::getJogadorID()const{
    return jogadorID;
}

// short Jogador::getPontuacao()const{
//     return pontuacao;
// }

// uint8_t Jogador::getDistanciaParaAtaqueEmArea()const{
//     return distanciaParaAtaqueEmArea;
// }

// uint8_t Jogador::getPoderDeAtaque()const{
//     return poderDeAtaque;
// }

// void Jogador::adicionarPontuacao( short pontuacao ){
//     this->pontuacao += pontuacao;
// }

void Jogador::atualizarJogador(){
    //atualizarPoderDeAtaque();
    //atualizarDistanciaParaAtaqueEmArea();
    mapa.atualizarTabuleiro();
}

short Jogador::getQuantidadeEmbarcacoesVivas(){
    if( getJogadorID() == JOGADOR_1 )
        return Embarcacao::getEmbarcacoesVivasJogador1();
    else
        return Embarcacao::getEmbarcacoesVivasJogador2();
}

void Jogador::inserirTabuleiroNaTela(){
    mapa.inserirTabuleiroNaTela();
}

ResultadoAcao * Jogador::atacarPosicaoTabuleiro_unico( ponto posicaoTabuleiro ){
    Objeto * objetoTemporario = mapa.getObjeto( posicaoTabuleiro );

    if( objetoTemporario == NULL )
        return new ResultadoAcao( _Flags_Objeto::VAZIO , ResultadoAcao::NEUTRALIZADO );
    
    return objetoTemporario->sofrerAtaque( posicaoTabuleiro , PODER_DE_ATAQUE_PADRAO );
}

// void Jogador::darPontos( ResultadoAcao * resultadoAtaque ){
//     if( resultadoAtaque->getTipoObjeto() == _Flags_Objeto::CANOA )
//         adicionarPontuacao( 5 );
//     else if( resultadoAtaque->getTipoObjeto() == _Flags_Objeto::SUBMARINO )
//         adicionarPontuacao( 10 );
//     else if( resultadoAtaque->getTipoObjeto() == _Flags_Objeto::PORTA_AVIAO && resultadoAtaque->getResultado() == ResultadoAcao::BEM_SUCEDIDO )
//         adicionarPontuacao( 20 );
// }


//CODIGO NERFADO POR QUESTOES DE PRAZO
// vector<ResultadoAcao * > * Jogador::atacarPosicaoTabuleiro_area( ponto posicaoTabuleiro ){
//     return floodFill( posicaoTabuleiro , getPoderDeAtaque() );
// }

// ResultadoAcao * Jogador::revelarPosicaoTabuleiro_unico( ponto posicaoTabuleiro ){
//     Objeto * objetoTemporario = mapa.getObjeto( posicaoTabuleiro );

//     if( objetoTemporario == NULL )
//         return new ResultadoAcao( _Flags_Objeto::VAZIO , ResultadoAcao::NEUTRALIZADO );
    
//     return objetoTemporario->tornarVisivel( posicaoTabuleiro );
// }

// vector<ResultadoAcao * > * Jogador::revelarPosicaoTabuleiro_area( ponto posicaoTabuleiro ){
//     return floodFill( posicaoTabuleiro );
// }