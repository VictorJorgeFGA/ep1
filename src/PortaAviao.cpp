#include "PortaAviao.hpp"
#include "Embarcacao.hpp"
#include "Objeto.hpp"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "ResultadoAcao.hpp"
#include "Orientacao.hpp"
#include "ponto.h"

#include <cstdio>
#include <cstdlib>

using namespace std;

short PortaAviao::portaAvioesJogador1 = 0;
short PortaAviao::portaAvioesJogador2 = 0;

uint8_t PortaAviao::habilidadeEspecial( uint8_t dano )const{
    int8_t chance = rand() % 3;
    if( chance ){
        printf("PortaAviao nao conseguiu segurar o missel.\n");
        return dano;
    }
    else
        return 0;
}

PortaAviao::PortaAviao( int8_t jogador , ponto posicao0 , Orientacao direcao )
: Embarcacao( jogador , posicao0 , direcao , _Flags_Objeto::CASAS_PORTAAVIAO ,
 _Flags_Objeto::VIDAS_PORTAAVIAO , _Flags_Objeto::PORTA_AVIAO ){

    switch( getJogador() ){
        case JOGADOR_1:
            portaAvioesJogador1++;
            break;
        case JOGADOR_2:
            portaAvioesJogador2++;
            break;
    }
}

PortaAviao::~PortaAviao(){
    switch ( getJogador() ){
        case JOGADOR_1:
            portaAvioesJogador1--;
            break;
    
        case JOGADOR_2:
            portaAvioesJogador2--;
            break;
    }
}

short PortaAviao::getPortaAvioesJogador1(){
    return portaAvioesJogador1;
}

short PortaAviao::getPortaAvioesJogador2(){
    return portaAvioesJogador2;
}

ResultadoAcao * PortaAviao::sofrerAtaque( ponto posicaoMatriz , uint8_t dano ){
    printf("PortaAviao sofrendo ataque.\n");
    tornarVisivel( procurarCasaCorrespondente( posicaoMatriz ) );
    return Embarcacao::sofrerAtaque( posicaoMatriz , habilidadeEspecial( dano ) );
}