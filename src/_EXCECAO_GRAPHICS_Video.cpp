#include "_EXCECAO_GRAPHICS_Video.hpp"

uint8_t excecoesOcorridas = 0;

_EXCECAO_GRAPHICS_Video::_EXCECAO_GRAPHICS_Video( const std::string & wht_msg )
:std::runtime_error( wht_msg ){ }

_EXCECAO_GRAPHICS_Video::_EXCECAO_GRAPHICS_Video( const char * wht_msg )
:std::runtime_error( wht_msg ){ }

uint8_t getExcecoesOcorridas(){
    return excecoesOcorridas;
}