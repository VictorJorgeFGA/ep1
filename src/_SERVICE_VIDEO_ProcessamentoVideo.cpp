#include "_SERVICE_VIDEO_ProcessamentoVideo.hpp"
#include "_EXCECAO_GRAPHICS_Video.hpp"
#include "_VIDEO_Textura.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include <cstdio>
#include <cstdint>
#include <string>

SDL_Window * _SERVICE_VIDEO_ProcessamentoVideo::janela = NULL;
SDL_Renderer * _SERVICE_VIDEO_ProcessamentoVideo::renderer = NULL;

void _SERVICE_VIDEO_ProcessamentoVideo::criarJanela( const char * tituloJanela , int16_t larguraJanela , int16_t alturaJanela ){
    janela = SDL_CreateWindow( tituloJanela , SDL_WINDOWPOS_CENTERED , SDL_WINDOWPOS_CENTERED ,
    larguraJanela , alturaJanela , SDL_WINDOW_SHOWN );

    if( janela == NULL ){
        printf("Erro ao criar janela em _SERVICE_VIDEO_ProcessamentoVideo::criarJanela.\n");
        throw _EXCECAO_GRAPHICS_Video( SDL_GetError() );
    }
}

void _SERVICE_VIDEO_ProcessamentoVideo::criarRenderer(){
    renderer = SDL_CreateRenderer( janela , -1 , SDL_RENDERER_ACCELERATED );

    if( renderer == NULL ){
        printf("Erro ao criar renderer em _SERVICE_VIDEO_ProcessamentoVideo::criarRenderer.\n");
        throw _EXCECAO_GRAPHICS_Video( SDL_GetError() );
    }
}

void _SERVICE_VIDEO_ProcessamentoVideo::iniciarRenderer(){
    if( SDL_SetRenderDrawColor( renderer , 0xFF , 0xFF , 0xFF , 0xFF ) != 0 ){
        printf("Erro ao colorir o renderer em _SERVICE_VIDEO_ProcessamentoVideo::iniciarRenderer.\n");
        throw _EXCECAO_GRAPHICS_Video( SDL_GetError() );
    }
}

void _SERVICE_VIDEO_ProcessamentoVideo::iniciarImagem(){
    int imgFlags = IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) ){
        printf("Erro ao iniciar o sistema de imagens PNG em _SERVICE_VIDEO_ProcessamentoVideo::iniciarImagem.\n");
        throw _EXCECAO_GRAPHICS_Video( IMG_GetError() );
    }
}

bool _SERVICE_VIDEO_ProcessamentoVideo::membrosEstaticosForamIniciados(){
    if( janela != NULL || renderer != NULL )
        return true;
    else
        return false;
}

void _SERVICE_VIDEO_ProcessamentoVideo::destruirRenderer(){
    SDL_DestroyRenderer( renderer );
    renderer = NULL;
}

void _SERVICE_VIDEO_ProcessamentoVideo::destruirJanela(){
    SDL_DestroyWindow( janela );
    janela = NULL;
}

void _SERVICE_VIDEO_ProcessamentoVideo::finalizarProcessamentoVideo(){
    IMG_Quit();
    SDL_Quit();
}

void _SERVICE_VIDEO_ProcessamentoVideo::iniciarVideo(){
    if( SDL_Init( SDL_INIT_VIDEO ) != 0 ){
        printf("Erro critico! Falha ao inicar o serviço de video SDL\n");
        throw _EXCECAO_GRAPHICS_Video( SDL_GetError() );
    }
}

_SERVICE_VIDEO_ProcessamentoVideo::_SERVICE_VIDEO_ProcessamentoVideo(){
    isAlias = true;
}

_SERVICE_VIDEO_ProcessamentoVideo::_SERVICE_VIDEO_ProcessamentoVideo( const char * tituloJanela , 
int16_t larguraJanela , int16_t alturaJanela ){

    if( membrosEstaticosForamIniciados() ){
        printf("Erro. Tentativa de dupla criacao de objetos de video em _SERVICE_VIDEO_ProcessamentoVideo::_SERVICE_VIDEO_ProcessamentoVideo.\n");
        throw _EXCECAO_GRAPHICS_Video( "Sobrecarga de Janela ou Renderer" );
    }

    isAlias = false;

    iniciarVideo();
    criarJanela( tituloJanela , larguraJanela , alturaJanela );
    criarRenderer();
    iniciarRenderer();
    iniciarImagem();
}

_SERVICE_VIDEO_ProcessamentoVideo::~_SERVICE_VIDEO_ProcessamentoVideo(){
    if( not isAlias ){
        destruirRenderer();
        destruirJanela();
        finalizarProcessamentoVideo();
    }
}

SDL_Renderer * _SERVICE_VIDEO_ProcessamentoVideo::getRenderer(){
    return renderer;
}

bool _SERVICE_VIDEO_ProcessamentoVideo::inserirTexturaNaTela( _VIDEO_Textura * textura ){
    if( SDL_RenderSetViewport( renderer , textura->getAreaTela() ) == 0 && 
        SDL_RenderCopy( renderer , textura->getTextura() , textura->getRecorteTextura() , NULL ) == 0 )
        return true;
    else
        return false;
}

bool _SERVICE_VIDEO_ProcessamentoVideo::limparTela(){
    if( SDL_RenderClear( renderer ) == 0 )
        return true;
    else
        return false;
}

void _SERVICE_VIDEO_ProcessamentoVideo::exibirTela(){
    SDL_RenderPresent( renderer );
}

void _SERVICE_VIDEO_ProcessamentoVideo::exibirFeedBackJogador( ResultadoAcao * acao ){
    if( acao->getResultado() == ResultadoAcao::NEUTRALIZADO )
        inserirTexturaNaTela( _Flags_Objeto::msg_malSucedido );

    else if( acao->getResultado() == ResultadoAcao::BEM_SUCEDIDO)
        inserirTexturaNaTela( _Flags_Objeto::msg_bemSucedido );

    if( acao->getTipoObjeto() == _Flags_Objeto::AGUA )
        inserirTexturaNaTela( _Flags_Objeto::msg_tiroAgua );
    
    else if( acao->getTipoObjeto() == _Flags_Objeto::CANOA )
        inserirTexturaNaTela( _Flags_Objeto::msg_tiroCanoa );

    else if( acao->getTipoObjeto() == _Flags_Objeto::SUBMARINO )
        inserirTexturaNaTela( _Flags_Objeto::msg_tiroSubmarino );

    else if( acao->getTipoObjeto() == _Flags_Objeto::PORTA_AVIAO )
        inserirTexturaNaTela( _Flags_Objeto::msg_tiroPortaAviao );
}

void _SERVICE_VIDEO_ProcessamentoVideo::exibirBackGround(){
    inserirTexturaNaTela( _Flags_Objeto::gameBackGround );
}