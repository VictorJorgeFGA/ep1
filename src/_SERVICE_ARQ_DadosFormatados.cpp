#include "_SERVICE_ARQ_DadosFormatados.hpp"
#include "Orientacao.hpp"
#include "Objeto.hpp"

#include "_Flags_Tabuleiro.hpp"
#include "_Flags_Objeto.hpp"

#include <string>
#include <cstdint>

using namespace std;

void _SERVICE_ARQ_DadosFormatados::converterEntrada_tipo( const string &tipo ){
    if( tipo == "canoa" )
        setTipo( _Flags_Objeto::CANOA );
    else if( tipo == "submarino" )
        setTipo( _Flags_Objeto::SUBMARINO );
    else if( tipo == "porta-avioes" )
        setTipo( _Flags_Objeto::PORTA_AVIAO );
    else
        setTipo( -1 );
}

void _SERVICE_ARQ_DadosFormatados::converterEntrada_direcao( const string &direcao ){
    if( direcao == "cima" )
        setDirecao( CIMA );
    else if( direcao == "baixo" )
        setDirecao( BAIXO );
    else if( direcao == "direita" || direcao == "nada" )
        setDirecao( DIREITA );
    else if( direcao == "esquerda" )
        setDirecao( ESQUERDA );
    else
        setDirecao( INVALIDA );
}

_SERVICE_ARQ_DadosFormatados::_SERVICE_ARQ_DadosFormatados( short i , short j , const string &tipo ,
 const string &direcao ){
    setI(i);
    setJ(j);

    converterEntrada_tipo( tipo );
    converterEntrada_direcao( direcao );
}

short _SERVICE_ARQ_DadosFormatados::getI()const{
    return i;
}

void _SERVICE_ARQ_DadosFormatados::setI( short i ){
    this->i = i;
}

short _SERVICE_ARQ_DadosFormatados::getJ()const{
    return j;
}

void _SERVICE_ARQ_DadosFormatados::setJ( short j ){
    this->j = j;
}

int8_t _SERVICE_ARQ_DadosFormatados::getTipo()const{
    return tipo;
}

void _SERVICE_ARQ_DadosFormatados::setTipo( int8_t tipo ){
    this->tipo = tipo;
}

Orientacao _SERVICE_ARQ_DadosFormatados::getDirecao()const{
    return direcao;
}

void _SERVICE_ARQ_DadosFormatados::setDirecao( Orientacao direcao ){
    this->direcao = direcao;
}

bool _SERVICE_ARQ_DadosFormatados::operator==( const _SERVICE_ARQ_DadosFormatados & objeto_dadosFormatados ){
    if( getI() == objeto_dadosFormatados.getI() && getJ() == objeto_dadosFormatados.getJ() 
        && getTipo() == objeto_dadosFormatados.getTipo() && getDirecao() == objeto_dadosFormatados.getDirecao() ){

        return true;
    } 
    else
        return false;
}

bool _SERVICE_ARQ_DadosFormatados::dadosSaoValidos()const{
    bool sucessFlag = true;
    //printf("_SERVICE_ARQ_DadosFormatados::dadosSaoValidos\n");
    
    if( i < 0 || i > MAX_LINHAS_TABULEIRO || j < 0 || j > MAX_COLUNAS_TABULEIRO ){
        sucessFlag = false;
        //printf("\t%hd ou %hd estao fora de intervalo.\n", i , j);
    }

    if( ! ( tipo == _Flags_Objeto::CANOA || tipo == _Flags_Objeto::SUBMARINO ||
            tipo == _Flags_Objeto::PORTA_AVIAO || tipo == _Flags_Objeto::AGUA )){
        
        //printf("\tTipo invalido %hd.\n", tipo);
        sucessFlag = false;
    }

    if( ! ( direcao == CIMA || direcao == BAIXO || direcao == DIREITA || direcao == ESQUERDA ) ){
        //printf("\tDirecao invalida.\n");
        sucessFlag = false;
    }
    
    //if(!sucessFlag) printf("\nDados invalidos no objeto DadosFormatados.\n");

    return sucessFlag;
}