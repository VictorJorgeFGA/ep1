#include "Agua.hpp"
#include "Objeto.hpp"
#include "Jogador.hpp"
#include "ponto.h"

#include "_Flags_Objeto.hpp"
#include "_VIDEO_Textura.hpp"
#include "ResultadoAcao.hpp"

Agua::Agua( int8_t jogador , ponto posicaoMatriz )
:Objeto( jogador , _Flags_Objeto::AGUA ) , posicaoMatriz( posicaoMatriz ){
    printf("Construtor Agua() iniciado:\n");
    textura = _Flags_Objeto::texturaIdealUnitaria( _Flags_Objeto::AGUA , _Flags_Objeto::VISIVEL , 0 ,
     DIREITA , posicaoMatriz );

    printf("Construtor Agua() executado com sucesso.\n");
}

Agua::~Agua(){
    delete textura;
}

ResultadoAcao * Agua::tornarVisivel( ponto posicaoMatriz ){
    return new ResultadoAcao( getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
}

ResultadoAcao * Agua::sofrerAtaque( ponto posicaoMatriz , uint8_t dano ){
    return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::NEUTRALIZADO );
}

ResultadoAcao * Agua::curar( ponto posicaoMatriz , uint8_t cura ){
    return new ResultadoAcao( Objeto::getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
}

ResultadoAcao * Agua::exibirNaTela( ponto posicaoMatriz ){
    //printarBloco( textura );
    return new ResultadoAcao( getTipoObjeto() , ResultadoAcao::BEM_SUCEDIDO );
}