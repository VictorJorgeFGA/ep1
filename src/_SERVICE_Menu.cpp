#include "_SERVICE_Menu.hpp"
#include "_EXCECAO_GRAPHICS_Video.hpp"
#include "_Flags_Video.hpp"

#include <cstdio>

void _SERVICE_Menu::iniciarTexturas( SDL_Renderer * renderer , const vector<string> & arquivosImagem ){
    if( arquivosImagem.size() != 3 ){
        printf("Erro! Quantidade de arquivos de textura invalida em _SERVICE_Menu::iniciarTexturas");
        throw _EXCECAO_GRAPHICS_Video( "Quantidade de arquivos invalida" );
    }
    mainMenu = new _VIDEO_Textura( renderer , arquivosImagem[0] );
    escolherModo = new _VIDEO_Textura( renderer , arquivosImagem[1] );
    escolherMapa = new _VIDEO_Textura( renderer , arquivosImagem[2] );
}

void _SERVICE_Menu::iniciarBotoes(  ){
    botaoIniciar.w = 255;
    botaoIniciar.h = 110;
    botaoIniciar.x = 50;
    botaoIniciar.y = _Flags_Video::ALTURA_TELA - 100 - botaoIniciar.h;

    botaoSair1.w = 255;
    botaoSair1.h = 110;
    botaoSair1.x = _Flags_Video::LARGURA_TELA - 50 - botaoSair1.w;
    botaoSair1.y = _Flags_Video::ALTURA_TELA - 100 - botaoSair1.h;

    botaoPvP.w = 450;
    botaoPvP.h = 60;
    botaoPvP.x = _Flags_Video::LARGURA_TELA - 50 - botaoPvP.w;
    botaoPvP.y = _Flags_Video::ALTURA_TELA - 250 - botaoPvP.h;

    botaoSair2.w = 255;
    botaoSair2.h = 110;
    botaoSair2.x = 10;
    botaoSair2.y = 40;

    botaoMapa1.w = 360;
    botaoMapa2.w = 360;
    botaoMapa3.w = 360;
    botaoMapa1.h = 490;
    botaoMapa2.h = 490;
    botaoMapa3.h = 490;

    botaoMapa1.x = 20;
    botaoMapa2.x = 420;
    botaoMapa3.x = _Flags_Video::LARGURA_TELA - 20 - botaoMapa3.w;

    botaoMapa1.y = _Flags_Video::ALTURA_TELA - 20 - botaoMapa1.h;
    botaoMapa2.y = botaoMapa1.y;
    botaoMapa3.y = botaoMapa2.y;

    botaoSair3 = botaoSair2;

    if( not botoesSaoValidos() ){
        printf("Erro. Botoes invalidos fornecidos em _SERVICE_Menu::iniciarBotoes.\n");
        throw _EXCECAO_GRAPHICS_Video( "Botao de dimensoes invalidas" );
    }
}

bool _SERVICE_Menu::botoesSaoValidos(){
    if( _Flags_Video::isAreaValida( &botaoIniciar ) && _Flags_Video::isAreaValida( &botaoSair1 ) 
    &&  _Flags_Video::isAreaValida( &botaoPvP ) && _Flags_Video::isAreaValida( &botaoSair2 )
    &&  _Flags_Video::isAreaValida( &botaoMapa1 ) && _Flags_Video::isAreaValida( &botaoMapa2 )
    &&  _Flags_Video::isAreaValida( &botaoMapa3 ) && _Flags_Video::isAreaValida( &botaoSair3 ) )
        return true;
    else
        return false;
}

Botao _SERVICE_Menu::verificarBotaoPressionado( ponto coordenadasClick ){
    Botao botaoPressionado = NENHUM;
    switch(telaMenuAtual){
        case MENU_PRINCIPAL:
            if( verificarColisao_Ponto_Rect( &botaoIniciar , &coordenadasClick ) )
                botaoPressionado = NOVO_JOGO;
            else if( verificarColisao_Ponto_Rect( &botaoSair1 , &coordenadasClick ) )
                botaoPressionado = SAIR;
            break;
        case ESCOLHA_MODO:
            if( verificarColisao_Ponto_Rect( &botaoPvP , &coordenadasClick ) )
                botaoPressionado = PVP;
            else if( verificarColisao_Ponto_Rect( &botaoSair2 , &coordenadasClick ) )
                botaoPressionado = SAIR;
            break;
        case ESCOLHA_MAPA:
            if( verificarColisao_Ponto_Rect( &botaoMapa1 , &coordenadasClick ) )
                botaoPressionado = ESCOLHER_MAPA_1;
            else if( verificarColisao_Ponto_Rect( &botaoMapa2 , &coordenadasClick ) )
                botaoPressionado = ESCOLHER_MAPA_2;
            else if( verificarColisao_Ponto_Rect( &botaoMapa3 , &coordenadasClick ) )
                botaoPressionado = ESCOLHER_MAPA_3;
            else if( verificarColisao_Ponto_Rect( &botaoSair3 , &coordenadasClick ) )
                botaoPressionado = SAIR;
            break;
    }
    return botaoPressionado;
}

bool _SERVICE_Menu::verificarColisao_Ponto_Rect( SDL_Rect * retangulo , ponto * pontoTela ){
    if( pontoTela->first >= retangulo->x && pontoTela->first <= retangulo->x + retangulo->w &&
        pontoTela->second >= retangulo->y && pontoTela->second <= retangulo->y + retangulo->h )
        return true;
    else{
        // printf("False pq: %hd >= %d e %hd <= %d e %hd >= %d e %hd <= %d eh falso.\n",
        // pontoTela->first, retangulo->x, pontoTela->first, retangulo->x + retangulo->w,
        // pontoTela->second, retangulo->y, pontoTela->second, retangulo->y + retangulo->h);
        return false;
    }
        
}

int8_t _SERVICE_Menu::atualizarMenu( Botao botaoPressionado ){
    int8_t efeitoClique = 0;

    switch( botaoPressionado ){
        case NOVO_JOGO:
            printf("Botao novo jogo pressionado.\n");
            telaMenuAtual = ESCOLHA_MODO;
            break;
        case SAIR:
            if( telaMenuAtual == MENU_PRINCIPAL ){
                efeitoClique = 1;
                telaMenuAtual = FORA_DO_MENU;
            }
            else if( telaMenuAtual == ESCOLHA_MODO ){
                telaMenuAtual = MENU_PRINCIPAL;
            }
            else if( telaMenuAtual == ESCOLHA_MAPA ){
                telaMenuAtual = ESCOLHA_MODO;
            }
            printf("Botao SAIR, pressionado.\n");
            break;
        case PVP:
            modoEscolhido = 0;
            telaMenuAtual = ESCOLHA_MAPA;
            printf("Botao PVP pressionado.\n");
            break;
        case ESCOLHER_MAPA_1:
            mapaEscolhido = 1;
            efeitoClique = 1;
            telaMenuAtual = FORA_DO_MENU;
            printf("Botao Mapa 1 pressionado.\n");
            break;
        case ESCOLHER_MAPA_2:
            mapaEscolhido = 2;
            efeitoClique = 1;
            telaMenuAtual = FORA_DO_MENU;
            printf("Botao Mapa 2 pressionado.\n");
            break;
        case ESCOLHER_MAPA_3:
            mapaEscolhido = 3;
            efeitoClique = 1;
            telaMenuAtual = FORA_DO_MENU;
            printf("Botao Mapa 3 pressionado.\n");
            break;
    }
    return efeitoClique;
}

_SERVICE_Menu::_SERVICE_Menu( SDL_Renderer * renderer , const vector<string> & arquivosImagem)
:telaMenuAtual( MENU_PRINCIPAL ), mapaEscolhido(-1), modoEscolhido(-1){
    iniciarTexturas( renderer , arquivosImagem );
    iniciarBotoes();
}

_SERVICE_Menu::~_SERVICE_Menu(){
    //printf("Destrutor de menu iniciado.\n");
    delete mainMenu;
    delete escolherModo;
    delete escolherMapa;
    printf("Destrutor de _SERVICE_Menu executado com sucesso.\n");
}

_VIDEO_Textura * _SERVICE_Menu::getTextura(){
    _VIDEO_Textura * ptr = NULL;
    switch( telaMenuAtual )
    {
        case MENU_PRINCIPAL:
            ptr = mainMenu;
            break;
        case ESCOLHA_MODO:
            ptr = escolherModo;
            break;
        case ESCOLHA_MAPA:
            ptr = escolherMapa;
            break;
        case FORA_DO_MENU:
            ptr = escolherMapa;
            break;
    }

    return ptr;
}

int8_t _SERVICE_Menu::inputClick( ponto coordenadasClick ){
    printf("ponto clicado: %hd,%hd\n", coordenadasClick.first , coordenadasClick.second);
    return atualizarMenu( verificarBotaoPressionado( coordenadasClick ) );
}

int8_t _SERVICE_Menu::modoDeJogoEscolhido()const{
    return modoEscolhido;
}

int8_t _SERVICE_Menu::mapaDeJogoEscolhido()const{
    return mapaEscolhido;
}

void _SERVICE_Menu::reiniciarMenu(){
    modoEscolhido = -1;
    mapaEscolhido = -1;
    telaMenuAtual = MENU_PRINCIPAL;
}