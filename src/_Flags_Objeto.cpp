#include "_Flags_Objeto.hpp"
#include "_Flags_Video.hpp"
#include "_VIDEO_Textura.hpp"
#include "Orientacao.hpp"

#include <cstdint>

_VIDEO_Textura * _Flags_Objeto::texturaCanoa       = NULL;
_VIDEO_Textura * _Flags_Objeto::texturaSubmarino   = NULL;
_VIDEO_Textura * _Flags_Objeto::texturaPortaAviao  = NULL;
_VIDEO_Textura * _Flags_Objeto::texturaAgua        = NULL;

_VIDEO_Textura * _Flags_Objeto::msg_VezJ1 = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_VezJ2 = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_tiroAgua = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_tiroCanoa = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_tiroPortaAviao = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_tiroSubmarino = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_bemSucedido = NULL;
_VIDEO_Textura * _Flags_Objeto::msg_malSucedido = NULL;
_VIDEO_Textura * _Flags_Objeto::gameBackGround = NULL;

void _Flags_Objeto::iniciarTexturas( SDL_Renderer * renderer ){
    texturaCanoa  = new _VIDEO_Textura( renderer , "doc/canoa.png" );
    texturaSubmarino = new _VIDEO_Textura( renderer , "doc/submarino.png" );
    texturaPortaAviao = new _VIDEO_Textura( renderer , "doc/porta_aviao.png" );
    //texturaAgua = new _VIDEO_Textura( renderer , "doc/agua.png" );

    SDL_Rect areaTela;

    areaTela.x = 10;
    areaTela.y = 10;
    areaTela.w = 845;
    areaTela.h = 845;
    texturaAgua = new _VIDEO_Textura( renderer , "doc/agua.png" , areaTela );

    areaTela.x = 914;
    areaTela.y = 281;
    areaTela.w = 241;
    areaTela.h = 72;
    msg_VezJ1 = new _VIDEO_Textura( renderer , "doc/msg_VezJ1.png" , areaTela );

    areaTela.x = 914;
    areaTela.y = 281;
    areaTela.w = 241;
    areaTela.h = 72;
    msg_VezJ2 = new _VIDEO_Textura( renderer , "doc/msg_VezJ2.png" , areaTela );

    //Agua - 131x60 - 959x549
    areaTela.x = 959;
    areaTela.y = 549;
    areaTela.w = 131;
    areaTela.h = 60;
    msg_tiroAgua = new _VIDEO_Textura( renderer , "doc/msg_tiroAgua.png" , areaTela );

    //Camada 1 171x72 - - 941BEx549BS
    areaTela.x = 941;
    areaTela.y = 549;
    areaTela.w = 171;
    areaTela.h = 72;
    msg_tiroCanoa = new _VIDEO_Textura( renderer , "doc/msg_tiroCanoa.png" , areaTela );

    //Porta Avião 270x62 - 902BEx549BS
    areaTela.x = 902;
    areaTela.y = 549;
    areaTela.w = 270;
    areaTela.h = 62;
    msg_tiroPortaAviao = new _VIDEO_Textura( renderer , "doc/msg_tiroPortaAviao.png" , areaTela );

    //Submarino 260x62 - 907BEx549BS
    areaTela.x = 907;
    areaTela.y = 549;
    areaTela.w = 260;
    areaTela.h = 62;
    msg_tiroSubmarino = new _VIDEO_Textura( renderer , "doc/msg_tiroSubmarino.png" , areaTela );

    //EM CHEIO!!! 270x62 - 892BEx643BS
    areaTela.x = 892;
    areaTela.y = 643;
    areaTela.w = 270;
    areaTela.h = 62;
    msg_bemSucedido = new _VIDEO_Textura( renderer , "doc/msg_bemSucedido.png" , areaTela );

    //NEUTRALIZADO 310x50 - 892BEx643BS
    areaTela.x = 892;
    areaTela.y = 643;
    areaTela.w = 310;
    areaTela.h = 50;
    msg_malSucedido = new _VIDEO_Textura( renderer , "doc/msg_malSucedido.png" , areaTela );

    gameBackGround = new _VIDEO_Textura( renderer , "doc/gameBackGround.png" );
}

void _Flags_Objeto::destruirTexturas(){
    delete texturaCanoa;
    delete texturaSubmarino;
    delete texturaPortaAviao;
    delete texturaAgua;
    delete msg_VezJ1,
    delete msg_VezJ2,
    delete msg_tiroAgua,
    delete msg_tiroCanoa,
    delete msg_tiroPortaAviao,
    delete msg_tiroSubmarino,
    delete msg_bemSucedido,
    delete msg_malSucedido,
    delete gameBackGround;
}

_VIDEO_Textura * _Flags_Objeto::texturaIdealUnitaria( int8_t tipo , int8_t estado, int8_t indice ,
Orientacao direcao , ponto posicaoMatriz ){
    printf("Criando texturaIdealUnitaria.\n");

    if( estado == VISIVEL )
        return texturaPadraoDesteTipo( tipo )->getParte( true , indice , direcao , posicaoMatriz );
    else if( estado == INVISIVEL )
        return texturaAgua->getParte( true , 0 , DIREITA , posicaoMatriz );
    else
        return texturaPadraoDesteTipo( tipo )->getParte( false , indice , direcao , posicaoMatriz );
}

_VIDEO_Textura * _Flags_Objeto::texturaPadraoDesteTipo( int8_t tipo ){
    _VIDEO_Textura * ptr = NULL;

    if( tipo == CANOA )
        ptr = texturaCanoa;
    else if( tipo == SUBMARINO )
        ptr = texturaSubmarino;
    else if( tipo == PORTA_AVIAO )
        ptr = texturaPortaAviao;
    else if( tipo == AGUA )
        ptr = texturaAgua;
        
    return ptr;
}

short _Flags_Objeto::vidasPadraoDesteTipo( int8_t tipo ){
    if( isTipoValido( tipo ) ){
        short vidas = 0;
        if( tipo == CANOA )
            vidas = VIDAS_CANOA;

        else if( tipo == SUBMARINO )
            vidas = VIDAS_SUBMARINO;
        
        else if( tipo == PORTA_AVIAO )
            vidas = VIDAS_PORTAAVIAO;

        //printf("Retornando vidas padrao.\n");
        return vidas;
    }   
    else
        return -1;
}

bool _Flags_Objeto::isTipoValido( int8_t flag ){
    bool sucessFlag = false;

    if( flag == CANOA || flag == SUBMARINO || flag == PORTA_AVIAO || flag == AGUA || flag == VAZIO )
        sucessFlag = true;
    
    return sucessFlag;
}