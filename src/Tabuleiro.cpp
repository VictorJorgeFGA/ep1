#include "Tabuleiro.hpp"
#include "Orientacao.hpp"

#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "ResultadoAcao.hpp"

#include "_SERVICE_VIDEO_ProcessamentoVideo.hpp"
#include "_SERVICE_ARQ_ProcessamentoMapa.hpp"
#include "_SERVICE_ARQ_DadosFormatados.hpp"

#include "_EXCECAO_ARQ_Invalido.hpp"

#include <fstream>
#include <vector>
#include <string>
#include <cstdio>

using namespace std;

void Tabuleiro::criarTabuleiro_mapaExistente( const string &caminhoArquivo ){
    _SERVICE_ARQ_ProcessamentoMapa mapaProcessado( caminhoArquivo , getJogador() );

    //Se o mapa processado nao possui estado valido
    if( !mapaProcessado ){
        remove( caminhoArquivo.c_str() );
        throw _EXCECAO_ARQ_Invalido( "EXCECAO: Arquivo com dados invalidos na construcao do tabuleiro." );
    }

    //Inicializa a matriz de objetos com NULL
    printf("Inicializando o tabuleiro de objetos com NULL.\n");
    for( int i = 0 ; i < getTamanho().first ; i++ )
        for( int j = 0 ; j < getTamanho().second ; j++ )
            setObjeto( NULL , i , j );

    printf("Criando objetos com base nos dados do arquivo.\n");
    for( short indiceInformacao = 0 ; indiceInformacao < mapaProcessado.getQuantidadeInformacoes() ; indiceInformacao++ ){
        short i     = mapaProcessado.getInformacao( indiceInformacao ).getI(),
              j     = mapaProcessado.getInformacao( indiceInformacao ).getJ();
        int8_t tipo = mapaProcessado.getInformacao( indiceInformacao ).getTipo();
        Orientacao direcao = mapaProcessado.getInformacao( indiceInformacao ).getDirecao();

        criarObjeto( ponto( i , j ) , tipo , direcao );
    }

    printf("Preenchendo o resto do tabuleiro com agua.\n");
    preencherTabuleiroComAgua();

    printf("Criacao do tabuleiro realizada com sucesso!\n");
}

//void Tabuleiro::criarTabuleiro_randomico(){}

void Tabuleiro::destruirTabuleiro(){
    _SERVICE_ARQ_ProcessamentoMapa mapaProcessado( caminhoArquivo , getJogador() );

    //Se por algum motivo o mapa foi corrompido
    if( !mapaProcessado ){
        throw _EXCECAO_ARQ_Invalido( "EXCECAO: Arquivo com dados invalidos na destruicao do tabuleiro." );
    }

    printf("Desalocando aguas:\n");
    //Desaloca todas as aguas
    for( int i = 0 ; i < getTamanho().first ; i++ ){
        for( int j = 0 ; j < getTamanho().second ; j++ ){
            if( getObjeto( i , j ) != NULL ){
                if( getObjeto( i , j )->getTipoObjeto() == _Flags_Objeto::AGUA  ){
                    printf("O objeto eh uma agua. Portanto destruir.\n");
                    destruirObjeto( ponto( i , j ) );
                }
            }
        }
    }
    printf("Agua desalocadas com sucesso\n");

    for( short indiceInformacao = 0 ; indiceInformacao < mapaProcessado.getQuantidadeInformacoes() ; indiceInformacao++ ){
        printf("Destruindo uma embarcacao.\n");
        
        short i = mapaProcessado.getInformacao( indiceInformacao ).getI();
        short j = mapaProcessado.getInformacao( indiceInformacao ).getJ();
        destruirObjeto( ponto( i , j ) );
    }
}

void Tabuleiro::preencherTabuleiroComAgua(){
    printf("Comecando a preencher o tabuleiro com agua:\n");
    for( short i = 0 ; i < getTamanho().first ; i++ ){
        for( short j = 0 ; j < getTamanho().second ; j++ ){
            if( getObjeto( i , j ) == NULL ){
                short x = j;
                short y = i;
                Agua * ptr = new Agua( getJogador() , ponto(x,y) );
                setObjeto( ptr , i , j );
            }
        }
    }

    printf("Tabuleiro preenchido com agua com sucesso.\n");
}

void Tabuleiro::criarObjeto( ponto posicaoMatriz , int8_t tipo , Orientacao direcao ){
    printf("Criando objeto.\n");
    posicionarNoTabuleiro( alocarObjeto( posicaoMatriz , tipo , direcao ) , posicaoMatriz , tipo , direcao );
}

void Tabuleiro::destruirObjeto( ponto posicaoMatriz ){
    //Retorno padrao para invalidez de intervalo
    if( getObjeto( posicaoMatriz ) == NULL )
        return;
    delete getObjeto( posicaoMatriz );
}

Objeto * Tabuleiro::alocarObjeto( ponto posicaoMatriz , int8_t tipo , Orientacao direcao ){
    printf("Alocando objeto.\n");
    Objeto * novoObjeto;

    if( tipo == _Flags_Objeto::CANOA )
        novoObjeto = new Canoa( getJogador() , posicaoMatriz , direcao );
    else if( tipo == _Flags_Objeto::SUBMARINO )
        novoObjeto = new Submarino( getJogador() , posicaoMatriz , direcao );
    else if( tipo == _Flags_Objeto::PORTA_AVIAO )
        novoObjeto = new PortaAviao( getJogador() , posicaoMatriz , direcao );
    else{
        printf("\nErro critico! Tipo invalido fornecido em alocarObjeto da classe Tabuleiro.\n");
        exit(1);
    }

    printf("Alocacao de objeto executada com sucesso.\n");
    return novoObjeto;
}

void Tabuleiro::posicionarNoTabuleiro( Objeto * objeto , ponto posicaoMatriz , int8_t tipo , Orientacao direcao ){
    printf("Posicionando no tabuleiro.\n");
    int8_t casas = -1;
    if( tipo == _Flags_Objeto::CANOA )
        casas = 1;
    else if( tipo == _Flags_Objeto::SUBMARINO )
        casas = 2;
    else if( tipo == _Flags_Objeto::PORTA_AVIAO )
        casas = 4;
    else{
        printf("\nErro critico! Tipo invalido fornecido em posicionarNoTabuleiro da classe Tabuleiro.\n");
        exit(1);
    }

    distribuirNoTabuleiro( objeto , posicaoMatriz , casas , direcao );
    printf("Posicionamento realizado com sucesso.\n");
}

void Tabuleiro::distribuirNoTabuleiro( Objeto * objeto , ponto pontoInicial , int8_t casas , Orientacao direcao ){
    printf("Distribuindo objeto no tabuleiro.\n");

    for( int indice = 0 ; indice < casas ; indice++ ){
        switch( direcao ){
            case CIMA:
            setObjeto( objeto , pontoInicial.first - indice , pontoInicial.second );
            break;
        case BAIXO:
            setObjeto( objeto , pontoInicial.first + indice , pontoInicial.second );
            break;
        case DIREITA:
            setObjeto( objeto , pontoInicial.first , pontoInicial.second + indice );
            break;
        case ESQUERDA:
            setObjeto( objeto , pontoInicial.first , pontoInicial.second - indice );
            break;
        }
    }

    printf("Distribuicao no tabuleiro executada com sucesso.\n");
}

void Tabuleiro::setObjeto( Objeto * objeto , ponto posicaoTabuleiro ){
    this->objeto[ posicaoTabuleiro.first * tamanho.second + posicaoTabuleiro.second ] = objeto; 
}

void Tabuleiro::setObjeto( Objeto * objeto , short i , short j ){
    this->objeto[ i * tamanho.second + j ] = objeto;
}

// Tabuleiro::Tabuleiro( int8_t jogador , ponto tamanho )
// :tamanho( tamanho ) , jogador( jogador ) , caminhoArquivo("") {

//     criarTabuleiro_randomico();
// }

Tabuleiro::Tabuleiro( int8_t jogador , const string &caminhoArquivo ) 
:tamanho( 13 , 13 ) , jogador( jogador ) , caminhoArquivo( caminhoArquivo ) {
    printf("Construtor Tabuleiro() iniciado.\n");
    criarTabuleiro_mapaExistente( caminhoArquivo );
}

Tabuleiro::~Tabuleiro(){
    printf("Destrutor do tabuleiro iniciado.\n");
    destruirTabuleiro();
    printf("Destrutor do tabuleiro executado com sucesso.\n");
}

Objeto * Tabuleiro::getObjeto( short i , short j )const{
    if( estaDentroTabuleiro( ponto( i , j ) ) )
        return objeto[ i * tamanho.second + j ];
    else
        return NULL;
}

Objeto * Tabuleiro::getObjeto( ponto posicaoMatriz )const{
    return getObjeto( posicaoMatriz.first , posicaoMatriz.second );
}

ponto Tabuleiro::getTamanho()const{
    return tamanho;
}

int8_t Tabuleiro::getJogador()const{
    return jogador;
}

Objeto * Tabuleiro::operator()( short i , short j )const{
    return getObjeto( i , j );
}

Objeto * Tabuleiro::operator()( ponto posicaoMatriz )const{
    return getObjeto( posicaoMatriz );
}

bool Tabuleiro::estaDentroTabuleiro( ponto posicaoMatriz )const{
    if( posicaoMatriz.first >= 0 && posicaoMatriz.first < getTamanho().first &&
        posicaoMatriz.second >= 0 && posicaoMatriz.second < getTamanho().second )
        return true;
    else
        return false;
}

void Tabuleiro::atualizarTabuleiro(){
    for( int i = 0 ; i < getTamanho().first ; i++ )
        for( int j = 0 ; j < getTamanho().second ; j++ )
            getObjeto( i , j )->atualizarObjeto();
}

void Tabuleiro::inserirTabuleiroNaTela(){
    _SERVICE_VIDEO_ProcessamentoVideo * video = new _SERVICE_VIDEO_ProcessamentoVideo();
    video->inserirTexturaNaTela( _Flags_Objeto::texturaAgua );

    for( int i = 0 ; i < tamanho.first ; i++ ){
        for( int j = 0 ; j < tamanho.second ; j++ ){
            if( getObjeto( i , j )->getTipoObjeto() != _Flags_Objeto::AGUA ){
                getObjeto( i , j )->exibirNaTela( ponto(i,j) );
            }
        }
    }
    if( jogador == JOGADOR_1 )
        video->inserirTexturaNaTela( _Flags_Objeto::msg_VezJ1 );
    else if( jogador == JOGADOR_2 )
        video->inserirTexturaNaTela( _Flags_Objeto::msg_VezJ2 );

    delete video;
}