#ifndef __EXCECAO_STD_FORADEINTERVALO_HPP_
#define __EXCECAO_STD_FORADEINTERVALO_HPP_

#include <stdexcept>
#include <utility>
#include <string>

//Msg padrao: EXCECAO: Indice fora de intervalo:

class _EXCECAO_STD_ForaDeIntervalo : public std::out_of_range{
    private:
        _EXCECAO_STD_ForaDeIntervalo();
    public:
        std::pair<short,short> intervalo;
        short ponto;

        _EXCECAO_STD_ForaDeIntervalo( const std::string & wht_msg , std::pair<short,short> intervalo,
        short ponto ) : std::out_of_range( wht_msg ) , intervalo( intervalo ) , ponto( ponto ) {}

        _EXCECAO_STD_ForaDeIntervalo( const std::string & wht_msg )
        : std::out_of_range( wht_msg ){ }
};

#endif