#ifndef _EMBARCACAO_HPP_
#define _EMBARCACAO_HPP_

#include "Objeto.hpp"
#include "CasaEmbarcacao.hpp"
#include "Orientacao.hpp"
#include "ponto.h"

#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "ResultadoAcao.hpp"
#include  "_VIDEO_Textura.hpp"

#include <vector>

using namespace std;

class Embarcacao : public Objeto{
    private:
        static short embarcacoesJogador1;
        static short embarcacoesJogador2;

        static short embarcacoesVivasJogador1;
        static short embarcacoesVivasJogador2;

        ponto       posicao0;
        Orientacao  direcao;
        bool        estadoVida;

        vector<CasaEmbarcacao * > casa;

        //Muda a flag de vida da embarcacao e dependendo do valor a ser atribuido
        //incrementa ou decrementa a quantidade de embarcacoes vivas
        void setEstadoVida( bool );

    protected:
        //FUNCOES UTILITARIAS

        //Procura a casa desta embarcacao que eh correspondente com a coordenada atacada
        short procurarCasaCorrespondente( ponto posicaoMatriz )const;

        //Verifica se o indice fornecido está dentro do intervalo de casas desta embarcacao
        bool verificarIndiceCasa( short indiceCasa )const;

        //Controi 'quantidadeCasas' casas, cada uma com 'vidasCasa' vidas.
        void construirCasas( short quantidadeCasas , short vidasCasa );

        //Torna visivel a i-esima casa desta embarcacao
        void tornarVisivel( short indiceCasa );

        //Restringe e resolve as dependencias de dados nas CasaEmbarcacao
        //Retorna quantas CasaEmbarcacao estao com estado igual MORTO
        short atualizarCasas();

        //Atualiza as texturas da embarcacao de acordo com o estado geral e o estado
        //das CasaEmbarcacao
        void atualizarTexturasEmbarcacao();

        void destruirCasasEmbarcacao();

    public:
        //Construtor
        Embarcacao( int8_t jogador = 0 , ponto posicao0 = ponto( 0 , 0 ), Orientacao = DIREITA ,
         uint8_t quantidadeCasas = 1 , uint8_t vidasCasa = 1 ,
         int8_t tipoObjeto = _Flags_Objeto::AGUA );

        //Destrutor
        virtual ~Embarcacao();

        //METODOS ACESSORES

        //Embarcacoes em instancia para o jogador1
        static short getEmbarcacoesJogador1();
        //Embarcacoes em instancia para o jogador 2
        static short getEmbarcacoesJogador2();

        static short getEmbarcacoesVivasJogador1();
        static short getEmbarcacoesVivasJogador2();

        CasaEmbarcacao * getCasa( short indiceCasa )const;

        //Adiciona um novo slot de casa a direita/baixo (Depende da direcao)
        void adicionarCasa( _VIDEO_Textura * , ponto , short );

        //Remove o slot mais a direita/baixo (Depende da direcao)
        void removerCasa();
        
        //Fornece a quantidade de casas que esta embarcacao possui atualmente
        short getQuantidadeCasasOnline()const;

        // //Retorna a i-esima casa desta embarcacao
        // CasaEmbarcacao operator()( short indice )const;

        //METODOS INTRINSECOS

        //Torna visivel a i-j-esima casa desta embarcacao
        virtual ResultadoAcao * tornarVisivel( ponto posicaoMatriz );

        //Resolve o ataque sofrido na i-j-ésima posicao do tabuleiro e retorna uma cte para o tipo
        //do objeto contido lá
        virtual ResultadoAcao * sofrerAtaque( ponto posicaoMatriz , uint8_t dano );

        // virtual ResultadoAcao * curar( ponto posicaoMatriz , uint8_t cura );

        //Exibe a i-j-esima casa do objeto na tela e retorna uma cte para o tipo do objeto
        //contido la
        virtual ResultadoAcao * exibirNaTela( ponto posicaoMatriz );

        //Versao atualizarObjeto para embarcacao
        virtual void atualizarObjeto();

        //Funcao predicado
        bool embarcacaoEstaViva();

        //Restringe e resolve todas as dependencias de dados
        void atualizarEmbarcacao();
};

#endif
