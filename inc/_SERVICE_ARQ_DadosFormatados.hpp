#ifndef _SERVICE_ARQ_DADOSFORMATADOS_HPP_
#define _SERVICE_ARQ_DADOSFORMATADOS_HPP_

#include "Orientacao.hpp"
#include <string>
#include <cstdint>

using namespace std;

class _SERVICE_ARQ_DadosFormatados{
    private:
        short   i,              //I-esima linha
                j;              //J-Esima coluna
        int8_t tipo;              //Tipo do objeto
        Orientacao direcao;     //Direcao da embarcacao

        void converterEntrada_tipo( const string & );
        void converterEntrada_direcao( const string & );
    
    public:
        _SERVICE_ARQ_DadosFormatados( short i , short j , const string &tipo , const string &direcao );

        //Retorna a linha da posicao inicial do objeto formatada
        short getI()const;
        //Atribui o valor fornecido ao I
        void setI( short );
        //Retorna a coluna da posicao inicial do objeto formatada
        short getJ()const;
        //Atribui o valor fornecido ao J
        void setJ( short );
        //Retorna o tipo do objeto formatado
        int8_t getTipo()const;
        //Atribui o valor fornecido ao tipo
        void setTipo( int8_t );
        //Retorna a direcao do objeto formatada
        Orientacao getDirecao()const;
        //Atribui a orientacao fornecida a direcao
        void setDirecao( Orientacao );
        //Verifica se dois dados formatados sao iguais
        bool operator==( const _SERVICE_ARQ_DadosFormatados & objeto_dadosFormatados );

        //METODOS INTRINSECOS
        //Verifica se os dados contidos no objeto obedecem a convencao de dados validos
        bool dadosSaoValidos()const;
};

#endif