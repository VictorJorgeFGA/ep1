#ifndef _CANOA_HPP_
#define _CANOA_HPP_

#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "Objeto.hpp"
#include "Orientacao.hpp"
#include "ponto.h"
#include "ResultadoAcao.hpp"

#include <cstdint>

using namespace std;

class Canoa : public Embarcacao{
    private:
        static short canoasJogador1;
        static short canoasJogador2;

    protected:
        //Utilitarios
        //virtual void atualizarEstadoVida();

    public:
        Canoa( int8_t jogador = 0 , ponto posicao0 = ponto(0,0),
        Orientacao direcao = DIREITA );

	    virtual ~Canoa();

        //METODOS ACESSORES

        static short getCanoasJogador1();
        static short getCanoasJogador2();
};

#endif
