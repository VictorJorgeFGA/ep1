#ifndef _SUBMARINO_HPP_
#define _SUBMARINO_HPP_

#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "Orientacao.hpp"
#include "ponto.h"

#include <cstdint>

using namespace std;

class Submarino : public Embarcacao{
    private:
        //ponto ataquesFataisSofridos;

        static short submarinosJogador1;
        static short submarinosJogador2;
        
        //CODIGO NERFADO
        //Resolve o ataque usando a habilidade especial
        //Retorna true se a habilidade especial foi utilizada contra o ataque
        //bool habilidadeEspecial( ponto posicaoMatriz );

    public:
        Submarino( int8_t jogador = 0 , ponto posicao0 = ponto( 0 , 0 ), Orientacao direcao = DIREITA );
        virtual ~Submarino();

        static short getSubmarinosJogador1();
        static short getSubmarinosJogador2();

        virtual ResultadoAcao * sofrerAtaque( ponto posicaoMatriz , uint8_t dano );
};

#endif