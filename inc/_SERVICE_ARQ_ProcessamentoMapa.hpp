#ifndef __SERVICE_ARQ_PROCESSAMENTO_HPP_
#define __SERVICE_ARQ_PROCESSAMENTO_HPP_

#include "_SERVICE_ARQ_DadosFormatados.hpp"
#include "Orientacao.hpp"
#include "_Flags_Jogador.hpp"
#include <fstream>
#include <vector>
#include <string>

using namespace std;

/*Classe para manipulacao de dados de mapa em arquivo.*/
class _SERVICE_ARQ_ProcessamentoMapa{
    private:
        vector<_SERVICE_ARQ_DadosFormatados> informacao;
        ifstream arquivoMapa;

        /* Este atributo se refere ao estado de execucao do objeto.
        Caso o objeto nao cumpra as normas de armazenamento de informacao
        ou tenha encontrado um erro durante o processamento, este atribuito
        tera valor logico falso*/
        bool possuiEstadoValido;

        //Setta a validade do arquivo
        void setValidade( bool possuiEstadoValido );
        //Retorna a posicao de onde comecam as informacoes do jogador no arquivo
        long procurarBlocoDoJogadorNoArquivo( short jogadorID );
        //Verifica se o indice fornecido esta dentro do intervalo do vetor de informacoes
        bool verificarIndice( short indice )const;
        //Verifica se o ID do jogador eh valido
        bool verificarJogadorID( short jogadorID );
        //Extrai os dados do arquivo a partir da posicao fornecida
        void extrairDados( long posicaoInicial );

        //Restricao do construtor padrao
        _SERVICE_ARQ_ProcessamentoMapa();

    public:
        _SERVICE_ARQ_ProcessamentoMapa( const string &caminhoArquivo , short jogadorID );
        //_SERVICE_ARQ_ProcessamentoMapa( const _SERVICE_ARQ_ProcessamentoMapa &objeto_processamentoMapa );
        ~_SERVICE_ARQ_ProcessamentoMapa();

        //Retorna a i-esima informacao extraida do mapa da parte do jogador
        _SERVICE_ARQ_DadosFormatados getInformacao( short indice )const;

        //Retorna true se o objeto tem problemas de validade de dados
        bool operator!()const;

        //Retorna quantas linhas de informacoes foram processadas
        short getQuantidadeInformacoes()const;

        //Operador de comparacao
        bool operator==( const _SERVICE_ARQ_ProcessamentoMapa & objeto_processamentoMapa );
};

#endif