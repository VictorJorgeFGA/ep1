#ifndef _TABULEIRO_HPP_
#define _TABULEIRO_HPP_

#include "Objeto.hpp"
#include "Embarcacao.hpp"
#include "CasaEmbarcacao.hpp"
#include "Agua.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "PortaAviao.hpp"
#include "ponto.h"
#include "Orientacao.hpp"
#include "ResultadoAcao.hpp"

#include "_Flags_Tabuleiro.hpp"
#include "_Flags_Objeto.hpp"

#include <string>
#include <vector>
#include <cstdint>

using namespace std;

class Tabuleiro{
    private:
        //Matriz de objetos
        Objeto * objeto[ MAX_TAMANHO_TABULEIRO ];
        //Tamanho em x e em y representado por um ponto fixo
        ponto tamanho;
        //Jogador detentor do tabuleiro
        int8_t jogador;
        //Caminho do mapa que contem as informacoes das embarcacoes do tabuleiro
        string caminhoArquivo;

        //CRIACAO DO TABULEIRO

        //Cria o tabuleiro com base em um arquivo de mapa existente
        void criarTabuleiro_mapaExistente( const string  &caminhoArquivo );

        //Cria o tabuleiro randomico
        //void criarTabuleiro_randomico( );

        void destruirTabuleiro();

        //Preenche todo o tabuleiro com uma unica agua alocada
        void preencherTabuleiroComAgua();

        //Aloca um objeto, do tipo fornecido, na i-j-esima posicao com a orientacao fornecida
        void criarObjeto( ponto posicaoMatriz , int8_t tipo , Orientacao direcao );

        //Desaloca o i-j-esimo objeto da lista
        void destruirObjeto( ponto posicaoMatriz );

        /*Aloca dinamicamente um novo objeto do tipo fornecido e retorna um ponteiro
        para ele */
        Objeto * alocarObjeto( ponto posicaoMatriz , int8_t tipo , Orientacao direcao );

        /*Posiciona no tabuleiro o todas as casas do objeto passado com base no tipo e direcao*/
        void posicionarNoTabuleiro( Objeto * objeto , ponto posicaoMatriz , int8_t tipo , Orientacao direcao );

        //Distribui o ponteiro do objeto de acordo com a quantidade de casas e o vetor direcao
        void distribuirNoTabuleiro( Objeto * objeto , ponto pontoInicial , int8_t casas , Orientacao direcao );
        
        void setObjeto( Objeto * objeto , ponto posicaoTabuleiro );
        void setObjeto( Objeto * objeto , short i , short j );



    public:
        //Construtor para tabuleiro randomicos
        //Tabuleiro( int8_t jogador );
        //Construtor para tabuleiros existentes
        Tabuleiro( int8_t jogador , const string &caminhoArquivo );
        //Destrutor para tabuleiro
        ~Tabuleiro();

        //Retorna o i-j-esimo objeto da matriz do tabuleiro
        Objeto * getObjeto( short i , short j )const;
        
        //Retorna o i-j-esimo objeto da matriz do tabuleiro
        Objeto * getObjeto( ponto posicaoMatriz )const;

        //Retorna o tamanho da matriz como um pair no formado <i,j>
        ponto getTamanho()const;

        //Retorna o jogador detentor do tabuleiro
        int8_t getJogador()const;

        //Operador de subescrito usando 2 shorts separados
        Objeto * operator()( short i , short j )const;
        //Operador de subescrito usando um ponto de matriz
        Objeto * operator()( ponto posicaoMatriz )const;

        //UTILITARIOS & INTRINSECOS
        
        //Verifica se o ponto fornecido esta dentro do tabuleiro
        bool estaDentroTabuleiro( ponto posicaoMatriz )const;

        //Atualiza o tabuleiro do jogo
        void atualizarTabuleiro();

        void inserirTabuleiroNaTela();
};

#endif