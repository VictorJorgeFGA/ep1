#ifndef _PORTAAVIAO_HPP_
#define _PORTAAVIAO_HPP_

#include "Embarcacao.hpp"
#include "Objeto.hpp"
#include "Orientacao.hpp"
#include "ponto.h"
#include "ResultadoAcao.hpp"

#include <cstdint>

using namespace std;

class PortaAviao : public Embarcacao{
    private:
        static short portaAvioesJogador1;
        static short portaAvioesJogador2;

        uint8_t habilidadeEspecial( uint8_t dano )const;

    public:
        PortaAviao( int8_t jogador = 0 , ponto posicao0 = ponto( 0 , 0),
        Orientacao direcao = DIREITA );
        virtual ~PortaAviao();

        static short getPortaAvioesJogador1();
        static short getPortaAvioesJogador2();

        virtual ResultadoAcao * sofrerAtaque( ponto posicaoMatriz , uint8_t dano );
};

#endif