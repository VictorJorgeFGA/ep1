#ifndef _OBJETO_HPP_
#define _OBJETO_HPP_

#include "ponto.h"
#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "_VIDEO_Textura.hpp"
#include "ResultadoAcao.hpp"

using namespace std;

class Objeto{
    private:
        static short objetosJogador1;
        static short objetosJogador2;

        int8_t jogador;
        int8_t tipoObjeto;

    protected:
        //UTILITARIOS

        //Printa um bloco de acordo com o tipo do objeto e com o estado fornecido
        void printarBloco( int8_t estadoObjeto )const;

        //Insere na tela a textura fornecida
        //Retorna true caso a textura seja inserida na tela corretamente
        bool printarBloco( _VIDEO_Textura * textura );

        //ACESSORES
        void setTipoObjeto( int8_t tipoObjeto );

    public:
        Objeto( int8_t jogador = 0 , int8_t tipoObjeto = _Flags_Objeto::AGUA );
        virtual ~Objeto();

        //METODOS ACESSORES

        static short getObjetosOnline();
        static short getObjetosJogador1();
        static short getObjetosJogador2();

        int8_t getJogador()const;
        void setJogador( short jogador );

        //Retorna uma cte para o tipo do objeto
        int8_t getTipoObjeto()const;

        //METODOS INTRINSECOS
        
        //Resolve o reveal da i-j-esima posicao do tabuleiro
        //Retorna um ponteiro para o ResultadoAcao gerado no metodo
        //O metodo que utilizar este servico, por favor, desalocar o resultado
        virtual ResultadoAcao * tornarVisivel( ponto posicaoMatriz ) = 0;

        //Resolve o ataque sofrido na i-j-ésima posicao do tabuleiro
        //Retorna um ponteiro para o ResultadoAcao gerado no metodo
        //O metodo que utilizar este servico, por favor, desalocar o resultado
        virtual ResultadoAcao * sofrerAtaque( ponto posicaoMatriz , uint8_t dano ) = 0;

        // //Heala o i-j-esimo objeto contindo no tabuleiro
        // //Retorna um ponteiro para o ResultadoAcao gerado no metodo
        // //O metodo que utilizar este servico, por favor, desalocar o resultado
        // virtual ResultadoAcao * curar( ponto posicaoMatriz , uint8_t cura ) = 0;

        //Exibe a i-j-esima casa do objeto na tela
        //Retorna um ponteiro para o ResultadoAcao gerado no metodo
        //O metodo que utilizar este servico, por favor, desalocar o resultado
        virtual ResultadoAcao * exibirNaTela( ponto posicaoMatriz ) = 0;

        //Atualiza, restringe e resolve todas as dependencias do objto
        virtual void atualizarObjeto();
};

#endif
