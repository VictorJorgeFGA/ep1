#ifndef __SERVICE_MENU_HPP_
#define __SERVICE_MENU_HPP_

#include <vector>
#include <string>
#include <cstdint>
#include "_VIDEO_Textura.hpp"
#include "ponto.h"

using namespace std;

enum EstagioMenu { MENU_PRINCIPAL , ESCOLHA_MODO , ESCOLHA_MAPA , FORA_DO_MENU };
enum Botao { NOVO_JOGO , SAIR , PVP , ESCOLHER_MAPA_1 , ESCOLHER_MAPA_2 , ESCOLHER_MAPA_3 , NENHUM };

class _SERVICE_Menu{
    private:
        _VIDEO_Textura * mainMenu;
        _VIDEO_Textura * escolherModo;
        _VIDEO_Textura * escolherMapa;

        SDL_Rect botaoIniciar;
        SDL_Rect botaoSair1;
        SDL_Rect botaoPvP;
        SDL_Rect botaoSair2;
        SDL_Rect botaoMapa1;
        SDL_Rect botaoMapa2;
        SDL_Rect botaoMapa3;
        SDL_Rect botaoSair3;

        int8_t mapaEscolhido;
        int8_t modoEscolhido;

        EstagioMenu telaMenuAtual;

        _SERVICE_Menu();

        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso a quantidade de arquivos
        //esteja errada
        void iniciarTexturas( SDL_Renderer * renderer , const vector<string> & arquivosImagem );

        //Inicia os botoes fornecidos
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso algum botao invalido
        //seja fornecido
        void iniciarBotoes( );

        //Verifica se os botoes sao validos(se estao dentro da tela)
        bool botoesSaoValidos();

        //verifica qual botao foi pressionado e retorna um enum do botao
        Botao verificarBotaoPressionado( ponto coordenadasClick );

        //Verifica se o ponto fornecido esta dentro do SDL_Rect fornecido
        //retorna true caso o ponto fornecido esteja dentro
        bool verificarColisao_Ponto_Rect( SDL_Rect * retangulo , ponto * pontoTela );

        //Atualiza o menu e retorna o efeito do clique
        //Retorna 0 se o efeito do clique nao resultou na saida do menu
        //Retorna 1 se o efeito do clique resultou na saida do menu
        int8_t atualizarMenu( Botao botaoPressionado );

    public:
        //Deve ser fornecido o renderer da tela atual
        _SERVICE_Menu( SDL_Renderer * renderer , const vector<string> & arquivosImagem );

        ~_SERVICE_Menu();

        //Retorna a atual textura a ser exibida
        _VIDEO_Textura * getTextura();

        //Processa o clique do usuario
        //Retorna 0 caso o clique nao tenha sido decidir deixar o menu
        //Retorna 1 caso o clique tenha sido decidir deixar o menu
        int8_t inputClick( ponto coordenadasClick );

        //Retorna o numero do modo de jogo escolhido
        //0 = P v P    1 = P v Cpu
        //-1 Ainda nao escolhido
        int8_t modoDeJogoEscolhido()const;

        //Retorna o numero do mapa escolhido
        //1 = Mapa 1    2 = Mapa 2      3 = Mapa 3
        //-1 Ainda nao escolhido
        int8_t mapaDeJogoEscolhido()const;

        void reiniciarMenu();
};

#endif