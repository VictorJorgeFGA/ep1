#ifndef __EXCECAO_GRAPHICS_VIDEO_HPP_
#define __EXCECAO_GRAPHICS_VIDEO_HPP_

#include <stdexcept>
#include <string>

class _EXCECAO_GRAPHICS_Video : public std::runtime_error {
    private:
        _EXCECAO_GRAPHICS_Video();
        static uint8_t excecoesOcorridas;
    public:
        explicit _EXCECAO_GRAPHICS_Video( const std::string & wht_msg );

        explicit _EXCECAO_GRAPHICS_Video( const char * wht_msg );
        
        //Retorna quantas excecoes deste tipo ocorreram
        static uint8_t getExcecoesOcorridas();
};

#endif