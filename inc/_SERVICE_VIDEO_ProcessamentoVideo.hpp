/*Classe destinada para manipulação e processamento de video e imagens */

/* Os dois principais membros de dados sao declarados como static para maior liberdade
de referencia a eles no codigo cliente. Esta classe deve ser instanciada com o construtor
de parametros apenas uma unica vez. Para gerar referencias, o construtor padrao vazio
deve ser utilizado */
#ifndef __SERVICE_VIDEO_PROCESSAMENTOVIDEO_HPP_
#define __SERVICE_VIDEO_PROCESSAMENTOVIDEO_HPP_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "_VIDEO_Textura.hpp"
#include "ResultadoAcao.hpp"

#include <cstdint>
#include <string>

using namespace std;

class _SERVICE_VIDEO_ProcessamentoVideo{
    private:
        static SDL_Window * janela;        //Estrutura janela
        static SDL_Renderer * renderer;    //Estrutura renderizadora

        //Flag verificadora de objeto
        //Possui valor verdadeiro se este objeto eh apenas um alias para o patriarca
        //Possui valor falso se este objeto eh o patriarca (construtor) dos membros de dados
        bool isAlias;

        //METODOS DE INICIALIZACAO

        //Cria uma nova janela SDL e insere seu endereco de memoria no membro 'janela'
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso ocorra algum erro
        void criarJanela( const char * tituloJanela , int16_t larguraJanela , int16_t alturaJanela );

        //Cria um novo renderer SDL que sera exibido na janela contida em 'janela'
        //e insere seu endereco de memoria no membro 'renderer'
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso ocorra algum erro
        void criarRenderer();

        //Faz as configuracoes iniciais do Renderer
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso ocorra algum erro
        void iniciarRenderer();

        //Iniciar o sistema de imagens SDL_image
        ////Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso ocorra algum erro
        void iniciarImagem();

        //Verifica se os membros de dados ja foram iniciados
        //Retorna true caso os membros ja foram iniciados
        bool membrosEstaticosForamIniciados();

        //METODOS DE SHUTDOWN

        //Destroi o renderer
        void destruirRenderer();

        //Destroi a janela
        void destruirJanela();

        //Finaliza todos os processamentos de imagem e video SDL
        void finalizarProcessamentoVideo();

        //Inicia os serviços SDL para video
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso ocorra algum erro
        void iniciarVideo();


    public:
        //Cria um alias para o patriarca
        _SERVICE_VIDEO_ProcessamentoVideo();

        //Este construtor so deve ser utilizado apenas uma unica vez.
        //Caso seja utilizado mais de uma vez, uma excecao do tipo _EXCECAO_GRAPHICS_Video
        //sera lancada
        _SERVICE_VIDEO_ProcessamentoVideo( const char * tituloJanela , int16_t larguraJanela ,
        int16_t alturaJanela );

        //Destroi os membros de dados static caso este objeto seja patriarca
        ~_SERVICE_VIDEO_ProcessamentoVideo();

        static SDL_Renderer * getRenderer();
        
        //Insere a textura fornecida na tela
        //Retorna true caso a textura seja inserida com sucesso
        static bool inserirTexturaNaTela( _VIDEO_Textura * textura );

        //Limpa a tela ( a deixa branca )
        //Retorna true caso a tela tenha sido limpa com sucesso
        static bool limparTela();

        //Exibe a tela em seu estado atual
        static void exibirTela();

        static void exibirFeedBackJogador( ResultadoAcao * acao );

        static void exibirBackGround();
};

#endif