#ifndef _BATALHANAVAL_HPP_
#define _BATALHANAVAL_HPP_

#include "Objeto.hpp"
#include "CasaEmbarcacao.hpp"
#include "Embarcacao.hpp"
#include "Canoa.hpp"
#include "Jogador.hpp"
#include "Submarino.hpp"
#include "PortaAviao.hpp"
#include "Agua.hpp"
#include "Orientacao.hpp"
#include "ponto.h"
#include "Tabuleiro.hpp"
#include "Jogador.hpp"
#include "ResultadoAcao.hpp"

#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "_Flags_Tabuleiro.hpp"
#include "_Flags_Video.hpp"

#include "_EXCECAO_ARQ_Invalido.hpp"
#include "_EXCECAO_STD_ForaDeIntervalo.hpp"
#include "_EXCECAO_GRAPHICS_Video.hpp"

#include "_SERVICE_ARQ_DadosFormatados.hpp"
#include "_SERVICE_ARQ_ProcessamentoMapa.hpp"
#include "_SERVICE_Menu.hpp"
#include "_SERVICE_VIDEO_ProcessamentoVideo.hpp"
#include "_VIDEO_Textura.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#endif