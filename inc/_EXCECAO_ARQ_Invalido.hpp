#ifndef __EXCECAO_ARQ_INVALIDO_HPP_
#define __EXCECAO_ARQ_INVALIDO_HPP_

#include <stdexcept>
#include <string>

class _EXCECAO_ARQ_Invalido : public std::runtime_error{
    private:
        _EXCECAO_ARQ_Invalido();

    
    public:
        _EXCECAO_ARQ_Invalido( const string & wht_msg = "Excecao do tipo Arquivo: Arquivo invalido"):
        std::runtime_error( wht_msg ){ }
};

#endif