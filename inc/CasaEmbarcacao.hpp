#ifndef _CASAEMBARCACAO_HPP_
#define _CASAEMBARCACAO_HPP_

#include "ponto.h"
#include "_VIDEO_Textura.hpp"

#include <cstdint>

using namespace std;

class CasaEmbarcacao{
    private:
        ponto posicaoMatriz;

        _VIDEO_Textura * textura;

        uint8_t  vidas,
                estado;

    protected:
        void setPosicaoMatriz( ponto posicaoMatriz );

    public:
	    CasaEmbarcacao( _VIDEO_Textura * textura , ponto posicaoMatriz = ponto( 0 , 0 ) ,
         uint8_t vidas = 1 );

         ~CasaEmbarcacao();

        //METODOS ACESSORES

        ponto getPosicaoMatriz()const;

        uint8_t getVidas()const;
        void setVidas( uint8_t vidas );

        uint8_t getEstado()const;
        void setEstado( uint8_t estado );

        _VIDEO_Textura * getTextura();
        void setTextura( _VIDEO_Textura * textura );

        //Desconta das vidas 'dano' unidades;
        void deduzirVidas( uint8_t dano );

        //Acrescenta 'cura' unidades de vida nesta casa
        void acrescentarVidas( uint8_t cura );
};

#endif
