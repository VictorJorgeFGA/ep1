#ifndef __VIDEO_TEXTURA_HPP_
#define __VIDEO_TEXTURA_HPP_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "Orientacao.hpp"
#include "ponto.h"

#include <string>
#include <cstdint>

using namespace std;

class _VIDEO_Textura{
    private:
        SDL_Texture * textura;

        //Area que sera capturada no recorte da textura
        SDL_Rect recorteTextura;
        //Area em que sera exibida a textura na tela
        SDL_Rect areaTela;

        /*Flag verificadora de objeto
        Possui valor true quando este objeto eh um alias pra uma SDL_Texture ja carregada
        Possui valor false quando este objeto eh um patriarca (construtor) para esta SDL_Texture*/
        bool isAlias;
        
        _VIDEO_Textura();

        //Cria uma nova SDL_Texture de acordo com o caminho de imagem fornecido
        //Setta os retangulos como full caso imagemCompleta seja true
        void criarTextura( SDL_Renderer * renderer , const string & caminhoImagem ,
          bool imagemCompleta = false );
        
        //Setta os retangulos de acorodo com os fornecidos no construtor
        void setRetangulos( SDL_Rect * areaTela , SDL_Rect * recorteTextura );

        //Verifica se osretangulos estao dentro da tela
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video
        void verificarTextura();

        //Destroi a SDL_Texture
        //Apenas texturas patriarcas podem utilizar este metodo
        void destruirTextura();

    public:
        //Construtor baseado em arquivo de imagem.
        //Este construtor cria a textura com a imagem completa
        //posicionada na tela com 0,0
        _VIDEO_Textura( SDL_Renderer * renderer , const string & caminhoImagem );

        _VIDEO_Textura( SDL_Renderer * renderer , const string & caminhoImagem ,
        SDL_Rect areaTela , SDL_Rect recorteTextura );

        _VIDEO_Textura( SDL_Renderer * renderer ,const string & caminhoImagem , SDL_Rect areaTela );

        //Construtor baseado em uma SDL_Texture existente, definindo uma nova area de recorte
        //e uma nova area de tela
        //Coordenadas da textura esta no formato linha coluna
        //Coordenadas da tela esta no formato x,y
        _VIDEO_Textura( SDL_Texture * texturaBase , ponto coordenadasTextura , ponto coordenadasTela );

        //Destroi a textura se e somente se este objeto eh um patriarca para esta SDL_Texture
        ~_VIDEO_Textura();

        //Retorna um ponteiro para o membro 'areaTela'
        SDL_Rect * getAreaTela();

        //Retorna um ponteiro para o membro 'recorteTextura'
        SDL_Rect * getRecorteTextura();

        //Retorna a i-esima casa desta textura, de acordo com a direcao fornecida, como uma nova textura.
        //A area da tela sera definida pela posicaoTabuleiro fornecida.
        //Retorna NULL caso a direcao seja invalida ou esta textura não seja a textura patriarca
        //Lanca uma excecao do tipo _EXCECAO_GRAPHICS_Video caso o indice ou a posicaoTabuleiro
        //sejam invalidos
        _VIDEO_Textura * getParte( bool estaVivo , int8_t indice , Orientacao direcao , ponto posicaoMatriz );

        //Retorna um ponteiro para a SDL_Texture desta textura
        SDL_Texture * getTextura()const;
};

#endif