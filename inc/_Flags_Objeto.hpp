/*Classe dedicada para flags de Objeto e seus serviços*/
#ifndef __FLAGS_OBJETO_HPP_
#define __FLAGS_OBJETO_HPP_

#include <cstdint>
#include "_VIDEO_Textura.hpp"
#include "Orientacao.hpp"

class _Flags_Objeto{
    private:
        //Bloqueio do construtor por decisao de arquitetura
        _Flags_Objeto();

    public:
        //Constante de limites
        static const short QNTD_MAX_OBJETOS = 400;

        //Constantes de objeto
        static const int8_t CANOA       = 31,  //Vermelho
                            SUBMARINO   = 34,  //Azul
                            PORTA_AVIAO = 35,  //Roxo
                            AGUA        = 36,  //Ciano
                            VAZIO       =  0;  //INCOLOR

        //Constantes de estado
        static const int8_t INVISIVEL  = 0,
                            VISIVEL    = 1,
                            MORTO      = 2;

        //Constantes particulares da canoa
        static const uint8_t VIDAS_CANOA = 1;
        static const uint8_t CASAS_CANOA =  1;

        //Constantes particulares do submarino
        static const uint8_t VIDAS_SUBMARINO = 2;
        static const uint8_t CASAS_SUBMARINO = 2;

        //Constante particulares do porta-aviao
        static const uint8_t VIDAS_PORTAAVIAO = 1;
        static const uint8_t CASAS_PORTAAVIAO = 4;

        //Texturas dos objetos
        static _VIDEO_Textura * texturaCanoa,
                              * texturaSubmarino,
                              * texturaPortaAviao,
                              * texturaAgua;
                              
        static _VIDEO_Textura * msg_VezJ1,
                              * msg_VezJ2,
                              * msg_tiroAgua,
                              * msg_tiroCanoa,
                              * msg_tiroPortaAviao,
                              * msg_tiroSubmarino,
                              * msg_bemSucedido,
                              * msg_malSucedido,
                              * gameBackGround;

        //Inicia as texturas estaticas
        static void iniciarTexturas( SDL_Renderer * renderer );

        //Destroi as texturas
        static void destruirTexturas();

        //Retorna uma nova textura ideal de 1 bloco para um objeto do tipo, estado e direcao fornecidos
        //Retorna NULL caso o tipo ou o estado ou a direcao sejam invalidos no contexto grafico
        static _VIDEO_Textura * texturaIdealUnitaria( int8_t tipo , int8_t estado , int8_t indice ,
         Orientacao direcao , ponto posicaoMatriz );

        //Retorna a textura padrao(viva) deste tipo
        //Retorna NULL caso o tipo fornecido seja invalido no contexto grafico
        static _VIDEO_Textura * texturaPadraoDesteTipo( int8_t tipo );
        
        //Retorna quantas vidas um objeto deste tipo teria por padrao
        //Retorna -1 caso o tipo especificado seja invalido
        //Retorna 0 caso o tipo seja AGUA ou VAZIO
        static short vidasPadraoDesteTipo( int8_t tipo );

        //Funcao predicado que verifica a validade da flag 
        static bool isTipoValido( int8_t flag );
};

#endif