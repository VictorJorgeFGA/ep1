#ifndef _RESULTADOACAO_HPP_
#define _RESULTADOACAO_HPP_

#include "_Flags_Objeto.hpp"

class ResultadoAcao{
    private:
        int8_t tipoObjeto;
        int8_t resultado;
        //O construtor padrao ficara bloqueado por questoes de arquitetura de codigo
        //A construcao de um novo ResultadoAcao devera ter argumentos explicitos
        ResultadoAcao();

    public:
        ResultadoAcao( int8_t tipoObjeto , int8_t resultado );

        //Retorna o tipo de objeto no qual a acao foi executada
        int8_t getTipoObjeto();

        //Retorna o resultado da acao
        //Retorna NEUTRALIZADO para acoes que nao ocorreram como desejado
        //Retorna BEM_SUCEDIDO para acoes que ocorreram como desejado
        int8_t getResultado();

        //Constantes

        static const int8_t NEUTRALIZADO = 1,
                            BEM_SUCEDIDO = 0;
};

#endif