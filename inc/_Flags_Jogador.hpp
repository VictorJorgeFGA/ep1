#ifndef __FLAGS_JOGADOR_HPP_
#define __FLAGS_JOGADOR_HPP_

#include <cstdint>

const int8_t COMPUTER  = 0;
const int8_t JOGADOR_1 = 1;
const int8_t JOGADOR_2 = 2;

#endif