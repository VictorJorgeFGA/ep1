#ifndef __FLAGS_VIDEO_HPP_
#define __FLAGS_VIDEO_HPP_

#include <cstdint>
#include <SDL2/SDL.h>

#include "ponto.h"

class _Flags_Video{
    private:
        //Bloqueio do construtor
        _Flags_Video();

    public:
        static const int16_t LARGURA_TELA = 1200,
                             ALTURA_TELA  = 865,
                             BORDA_ESQUERDA = 10,
                             BORDA_SUPERIOR = 10;


        //W e H nao serao alteradas para 65 pois o viewport comprimira a imagem
        static const int16_t wImg; //Largura padrao em pixels de um bloco de imagem
        static const int16_t hImg; //Altura padrao em pixels de um bloco de imagem

        static const int16_t wTel;
        static const int16_t hTel;

    //Verifica se este retangulo representa um recorte valido na textura
    //Retorna true caso o retangulo for valido
    static bool isRecorteValido( SDL_Rect * retangulo );

    //Verifica se este retangulo representa uma area valida na tela
    //Retorna true caso o retangulo for valido
    static bool isAreaValida( SDL_Rect * retangulo );

    //Retorna a coordenada do pixel da posicao do tabuleiro requisitada
    static ponto pixelDestaPosicaoTabuleiro( ponto posicaoTabuleiro );

    static ponto posicaoTabuleiroDestePixel( ponto pixel );
};

#endif