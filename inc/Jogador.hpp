#ifndef _JOGADOR_HPP_
#define _JOGADOR_HPP_

#include "Tabuleiro.hpp"
#include "ResultadoAcao.hpp"

#include "_Flags_Jogador.hpp"
#include "_Flags_Objeto.hpp"
#include "_Flags_Tabuleiro.hpp"

#include <string>
#include <vector>

using namespace std;

class Jogador{
    private:
        const int8_t jogadorID;
        //short pontuacao;
        //uint8_t poderDeAtaque;
        //uint8_t distanciaParaAtaqueEmArea;
        string nomeJogador;
        Tabuleiro mapa;

        static const uint8_t PODER_DE_ATAQUE_PADRAO;
        //static const uint8_t DISTANCIA_PARA_ATAQUE_EM_AREA_PADRAO;
        //static const uint8_t DIVISOR_PODER_DE_ATAQUE;
        //static const uint8_t DIVISOR_DISTANCIA_PARA_ATAQUE_EM_AREA;

        //Bloqueando o construtor padrao
        Jogador();

        // //Verifica se este ponto esta dentro do intervalo do tabuleiro
        // //Deduz do dano a potencia consumida pela distancia do ponto inicial
        // uint8_t deduzirPotenciaDano( ponto pontoAtual , ponto pontoOrigem );

        //CODIGO NERFADO POR QUESTOES DE PRAZO
        // /*Ataca a maior area possivel para este jogador. Se a potencia do ataque for nula
        // o metodo revelara a area sem causar dano as embarcacoes. Retorna um ponteiro para
        // um vector de flags do que foi atacado ou revelado ou returna NULL se nada foi atacado*/
        // vector<ResultadoAcao * > * floodFill( ponto pontoInicial , uint8_t potenciaAtaque = 0 );
        
        // //Atualiza o poderDeAtaque com base na pontuacao do jogador
        // void atualizarPoderDeAtaque();
        
        //CODIGO NERFADO POR QUESTOES DE PRAZO
        // //Atualiza a distanciaParaAtaqueEmArea com base na pontuacao do jogador
        // void atualizarDistanciaParaAtaqueEmArea();
        

    public:
        //Jogador( int8_t jogadorID = COMPUTER , const string & nomeJogador = "" );

        Jogador( int8_t jogadorID = COMPUTER , const string & caminhoArquivoMapa = "" , const string & nomeJogador = "" );

        //METODOS ACESSORES

        string getNomeJogador()const;
        void setNomeJogador( const string & nomeJogador );

        int8_t getJogadorID()const;
        /*Este metodo ficara bloqueado pois a modificacao do ID do jogador sera feita 
        apenas pelo construtor*/
        //void setJogadorID( const short & );

        // //Retorna a pontuacao atual do jogador
        // short getPontuacao()const;

        // //Retorna o range maximo atual para ataques em area deste jogador
        // uint8_t getDistanciaParaAtaqueEmArea()const;

        // //Retorna o poder de ataque maximo atual deste jogador
        // uint8_t getPoderDeAtaque()const;

        // void adicionarPontuacao( short pontuacao );

        //METODOS INTRINSECOS
        //Restringe e resolve todas as dependencias de dados
        void atualizarJogador();

        //Retorna quantas embarcacoes vivas restam no tabuleiro deste jogador
        short getQuantidadeEmbarcacoesVivas();

        //Exibe o tabuleiro
        void inserirTabuleiroNaTela();

        //Ataca a i-j-esima posicao do tabuleiro
        //Retorna um ponteiro para o resultado da acao
        ResultadoAcao * atacarPosicaoTabuleiro_unico( ponto posicaoTabuleiro );

        // void darPontos( ResultadoAcao * );

        //CODIGO NERFADO POR QUESTOES DE PRAZO
        // /*Ataca uma area que abrange d casas da i-j-esima posicao em todas as direcoes
        // e retorna um ponteiro para um vector de ponteiro resultados de acao. A funcao que solicita este serviço
        // deverá desalocar a lista */
        // vector<ResultadoAcao * > * atacarPosicaoTabuleiro_area( ponto posicaoTabuleiro );
        // //Revela a i-j-esima posicao do tabuleiro
        // //Retorna um ponteiro para o resultado da acao
        // ResultadoAcao * revelarPosicaoTabuleiro_unico( ponto posicaoTabuleiro );
        // /*Revela uma area de d casas da i-j-esima posicao em todas as direcoes e retorna
        // um ponteiro para um vector de ponteiro resultados de acao. A funcao que solicita este serviço
        // deverá desalocar a lista */
        // vector<ResultadoAcao * > * revelarPosicaoTabuleiro_area( ponto posicaoTabuleiro );
};

#endif
