#ifndef _AGUA_HPP_
#define _AGUA_HPP_

#include "Objeto.hpp"
#include "ponto.h"
#include "ResultadoAcao.hpp"

#include <utility>
#include <cstdint>

using namespace std;

class Agua : public Objeto{
    private:
        _VIDEO_Textura * textura;
        ponto posicaoMatriz;

    public:
        Agua( int8_t jogador , ponto posicaoMatriz );
        virtual ~Agua();

        virtual ResultadoAcao * tornarVisivel( ponto posicaoMatriz );

        virtual ResultadoAcao * sofrerAtaque( ponto posicaoMatriz , uint8_t dano );

        virtual ResultadoAcao * curar( ponto posicaoMatriz , uint8_t cura );

        virtual ResultadoAcao * exibirNaTela( ponto posicaoMatriz );
};

#endif
