# EP1 - OO 2019.1 (UnB - Gama) - Batalha Naval

&nbsp;&nbsp;O jogo consiste numa versao do classico game Batalha Naval, contando com 3 embarcações diferentes: canoa, submarino e porta-avião.

&nbsp;&nbsp;Infelizmente, por questões de prazo, o projeto está longe de estar acabado ou em um estado consistente. Porém, experiências em que nem tudo acaba como desejado podem nos ensinar muito mais do que um completo sucesso.

&nbsp;&nbsp;O projeto conta com um menu interativo que recebe inputs do mouse (cliques esquerdos). Conta com uma area de jogo com um tabuleiro 13x13 que alterna de acordo com a vez do jogador. Toda a interação usuário-game é feita com cliques. Os ataques são realizados clicando em qualquer lugar do tabuleiro. É informado ao jogador o que a ação dele gerou no tabuleiro atacado. As ações são: Tiro na Agua, Tiro na Canoa, Tiro no Submarino e Tiro no Porta-Aviao, podendo estas ações serem Bem sucedidas( Em cheio, para o jogador ) ou mal sucedidas ( Neutralizado, para o jogador ).

&nbsp;&nbsp;Vale ressaltar que os arquivos de mapa devem ser nomeados com "map_X.txt" com X sendo 1, 2 ou 3. Os arquvios de imagem PNG não devem ser alterados em nenhuma hipótese. O programa não terá robustez de lidar com erros de entrada de arquivo.

&nbsp;&nbsp;O game conta com uma interface gráfica gerada pela lib SDL2 e SDL2 image.

## Instalando SDL2 e SDL2 image

## * Para Linux

&nbsp;&nbsp;&nbsp; SDL2:

&nbsp;&nbsp;Você precisará de privilégios de root para instalar os pacotes, então certifique-se de usar "su" ou "sudo" se você não estiver logado como root.

Certifique-se de ter atualizado para a versão mais recente da sua distro porque o gerenciador de pacotes antigo pode não suportar a versão mais recente do SDL 2.

1) Para aqueles de vocês que têm Advanced Packaging Tool disponível (ou seja, Ubuntu e Debian) você vai querer procurar o cache do apt-get para encontrar a versão atual da SDL 2 para instalar. Você pode procurar os pacotes apt-get disponíveis usando o comando:

```
$ apt-cache search libsdl2
```

Você vai precisar baixar a versão de desenvolvimento do SDL 2. Você pode instalar este pacote usando o comando:
```
$ apt-get install libsdl2-dev
```

Se você usa o Yellow dog Updater, Modificado (usado no Fedora e no CentOS), você pode digitar o comando:
```
$ yum search SDL2-devel
```

Para instalar, execute o seguinte comando:
```
$ yum install SDL2-devel
```
IMPORTANTE: O cabeçalho SDL.h deve estar contido numa pasta chamada SDL2, dependendo da sua máquina o cabeçalho por default será posto na pasta SDL2, de tal maneira que para incluir o cabeçalho no código será usado #include <SDL2/SDL.h>

Caso a instalação não seja possível ou não ocorra de forma bem sucedida, confira a forma de instalação para Linux completa [aqui](http://lazyfoo.net/tutorials/SDL/01_hello_SDL/linux/index.php).

&nbsp;&nbsp;&nbsp; SDL2_image:

&nbsp;&nbsp;Você precisará de privilégios de root para instalar os pacotes, então certifique-se de usar "su" ou "sudo" se você não estiver logado como root. Execute os comandos:

```
$ apt-cache search libsdl2-image
$ apt-get install libsdl2-image-dev
```
Para o YellowDog:

```
$ yum search SDL2_image-devel
$ yum install SDL2_image-devel
```
IMPORTANTE: O cabeçalho SDL_image.h deve estar contido numa pasta chamada SDL2, dependendo da sua máquina o cabeçalho por default será posto na pasta SDL2, de tal maneira que para incluir o cabeçalho no código será usado #include <SDL2/SDL_image.h>

Caso a instalação não seja possível ou não ocorra de forma bem sucedida, confira a forma de instalação para Linux completa [aqui](http://lazyfoo.net/tutorials/SDL/06_extension_libraries_and_loading_other_image_formats/linux/index.php).

## * Para Mac OS

&nbsp;&nbsp;A maneira mais fácil de se instalar as libs SDL2 no Mac OS é utilizando package manager HomeBrew. Ele não vem instalado por padrão, mas sua instalação é simples e você pode ver como fazê-la [aqui](https://brew.sh/index_pt-br). Para instalar o SDL2 e o SDL2_image execute os comandos:

```
$ brew install sdl2
```

```
$ brew install sdl2_image
```

## * Para Windows

&nbsp;&nbsp;Infelizmente não tratei de trazer suporte para Windows.

## Executando

&nbsp;&nbsp;Para executar o programa, primeiro compile os códigos utilizando:
```
$ make
```

Depois use, para executar:
```
$ make run
```

Pode-se usar também o make clean para limpar os arquivos gerados pelo make e recompilar o código:
```
$ make clean
```